package Task4;

import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Clasa chest este folosita pentru a pastra ingredientele din cub. Am separat-o de restul implementarii pentru a putea sincroniza accesul la ea. Metodele de care dispune sunt:
 * <p>{@link #Chest(int,String[])} constructorul clasei</p>
 * <p>{@link #LoseIngredients()}</p>
 * <p>{@link #addIngredient(Demon) }</p>
 * <p>{@link #takeIngredients(int[], Witch)}</p>
 * <p>Membrii de tip data sunt:</p>
 * <p>ingredientsBuffer - ingredientele din cuib create de demoni</p>
 * <p>timesRead - numarul de citiri efectuate de vrajitoare</p>
 * <p>lockWitch - lock pentru vrajitoare</p>
 * <p>lockDemon - lock pentru demon</p>
 * <p>lockUndead - lock pentru strigoi</p>
 * <p>ingredientsName - lista cu numele fiecarui ingredient</p>
 * <p>idCoven - idul cuibului din care face parte cufarul</p>
 * @author Marinescu Ana
 *
 */
public class Chest {
	private int[] ingredientsBuffer=new int[10];//ingredientele din cuib create de demoni
	private int timesRead=0;//numarul de citiri efectuate de vrajitoare
	private Lock lockWitch = new ReentrantLock();//lock pentru vrajitoare	
	private Lock lockDemon = new ReentrantLock();//lock pentru demon
	private Lock lockUndead = new ReentrantLock();// lock pentru strigoi
	private String ingredientsName[];//lista cu numele fiecarui ingredient
	private int idCoven;//idul cuibului din care face parte cufarul
	
	/**
	 * Constructorul care initializeaza variabilele cu acelasi nume:
	 * @param idCoven idul cuibului din care face parte cufarul
	 * @param ingredientsName lista cu numele fiecarui ingredient
	 */
	public Chest(int idCoven,String ingredientsName[]) {
		this.ingredientsName=ingredientsName;
		this.idCoven=idCoven;
	}
	
	/**
	 * Atunci cand un strigoi acceseaza cuibul, in cazul in care nicio vrajitoare nu se lupta cu el, cuibul pierde ingredientele. Pentru asta, se blocheaza cufarul pentru restul threadurilor,
	 * se itereaza prin ingredientele bufferului initializandu-le pe toate cu 0, apoi se elibereaza cufarul.
	 */
	public void LoseIngredients()
	{
		lockUndead.lock();
		
		for(int ingredient:ingredientsBuffer)
		{
			ingredient=0;
		}
		
		lockUndead.unlock();
	}
	
	/**
	 * Pentru a adauga ingrediente, demonul blocheaza cufarul pentru restul threadurilor, se alege aleator un ingredient pe care sa il creeze demonul primit ca parametru,
	 * noIngredients de acel tip sunt create si adaugate in cufar, dupa care se elibereaza cufarul. 
	 * @param demon demonul care adauga ingredientele
	 */
	public void addIngredient(Demon demon) {
		
		lockDemon.lock(); 

		try {
			
			Random objectRandom=new Random();
			
			int typeIngredient=objectRandom.nextInt(10);
			
			ingredientsBuffer[typeIngredient]+=demon.GetNoIngredients();
			
			System.out.println("Demon "+demon.GetId() +" in coven "+idCoven+" created "+ demon.GetNoIngredients()+" "+ingredientsName[typeIngredient]+" ingredients.");
		
		}
		finally {
		
			lockDemon.unlock(); 
			
		}

	}
	
	/**
	 * Atunci cand o vrajitoare acceseaza cuibul aceasta ia ingrediente. Pentru asta, se blocheaza cufarul pentru restul threadurilor. O vrajitoare poate lua min ingrediente din cele care ii trebuie(adica din cele aflate in lista data ca parametru).
	 * Daca se afla la doua citire din cufar, atunci se asteapta un timp [n milisecunde, unde n apartine intervalului[0,100]] pana cand aceasta poate citii. Se initializeaza min cu numarul de ingrediente disponibile din primul tip necesar vrajitoarei,
	 * apoi se itereaza prin buffer pana se determina minimul. Odata ce se stie cate ingrediente din fiecare ingredient din lista vrajitoarei va fi extras, se extrage (prin decrementare cu min) si se adauga in geanta vrajitoarei (prin incrementare cu min).
	 * Se marcheaza faptul ca a fost citit din buffer prin incrementarea lui timesRead, se elibereaza cufarul dand permisii celorlalte threaduri si se returneaza numarul de ingrediente extrase din fiecare.
	 * 
	 * @param ingredientList lista de ingrediente de care are nevoie vrajitoarea
	 * @param witch vrajitoarea care urmeaza sa extraga ingrediente din cuib
	 * @return -1 daca nu s-a putut extrage niciun ingredient, sau un numar natural daca au fost extrase ingrediente
	 */
	public int takeIngredients(int[] ingredientList, Witch witch) {

		
		lockWitch.lock();
		
		int min=-1;

		try {
		
			if(timesRead==2)
				{
					Random objectRandom=new Random();
					
					Thread.sleep(objectRandom.nextInt(100));
					
					timesRead=0;
				}
			
			min=ingredientsBuffer[ingredientList[0]];
			
			for(int ingredientType:ingredientList)
			{				
				if(ingredientsBuffer[ingredientType]<min && ingredientsBuffer[ingredientType]!=0)
					{
						min=ingredientsBuffer[ingredientType];
					}
			}
			
			for(int ingredientType:ingredientList)
			{				
				if(ingredientsBuffer[ingredientType]!=0)
					{
						ingredientsBuffer[ingredientType]-=min;
						witch.magicBag[ingredientType]+=min;
					}
			}
			
			timesRead++;
			
		}catch (Exception e) {
			
			e.printStackTrace();
			
			return min;
		
		}finally {
		
			lockWitch.unlock();
		}
			
		return min;
	}
	
}
