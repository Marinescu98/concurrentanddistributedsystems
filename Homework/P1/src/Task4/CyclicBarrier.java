package Task4;

/**
 * Clasa CyclicBarrier reprezinta o adapteare a clasei CyclicBarrier din pachetul de concurenta pe care java ni-l pune la dispozitie. Metodele implementate sunt:
 * <p>{@link #CyclicBarrier(Coven)}</p>
 * <p>{@link #StopBarrier(Demon)}</p>
 * <p>Datele de tip membru sunt:</p>
 * <p>coven - instanta a cuibului a carui matrice trebuie sincronizati demonii odata ce ajung la bariera</p>
 * <p>partiesAwait - numarul de demoni aflati in asteptare in bariera</p>
 * @author Marinescu Ana
 *
 */
public class CyclicBarrier {
	   
	Coven coven;
    int partiesAwait; 
    
    /**
     * Constructorul clasei
     * @param coven instanta a cuibului a carui matrice trebuie sincronizati demonii odata ce ajung la bariera
     */
	public CyclicBarrier(Coven coven) {
		this.coven=coven;
		this.partiesAwait=0;
	}
	
	/**
	 * Metoda primeste ca parametru un demon care odata ce intra in bariera inseamna ca se afla pe diagonala principala. Se incrementeaza variabila
	 * care numara cati demoni asteapta in bariera, se seteaza variabila booleana din Demon care determina daca demonul se afla in bariera astfel 
	 * incat sa nu mai produca ingrediente, si cat timp demonii din lista nu au ajuns la bariera asteapta. Odata ajunsi sunt eliberati.
	 * @param demon demonul care a ajuns la bariera
	 */
	public synchronized void StopBarrier(Demon demon) {
		
		System.out.println("The demon"+demon.GetId()+" is arrived to barrier.");
		
		partiesAwait++;
		
		demon.SetArriveToBarrier(true);
		
		try {
			
			if(partiesAwait<coven.GetSizeListDemons())
			 {
				wait();
			 
			 }else {
				
				 partiesAwait=0;
				 
				 notifyAll();
				 
				 System.out.println("The demon"+demon.GetId()+" left the barrier.");
				 
				 demon.SetArriveToBarrier(false);
			}
		
		}catch (Exception e) {
		
			e.printStackTrace();
		
		}
		
		
		
	}
}
