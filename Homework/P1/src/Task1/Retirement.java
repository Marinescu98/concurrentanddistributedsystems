package Task1;

import java.util.Random;
import java.util.concurrent.Semaphore;

/**
 * Clasa reprezinta firul de pensionare pe care demonii il pot accesa pentru a cere permisii de iesit la pensie si care la un anumit interval de timp,
 * retrage demoni din cuib. Metodele implementate sunt:
 * <p>{@link #Retirement(Coven, War)}</p>
 * <p>{@link #run()}</p>
 * <p>{@link #RequestRetirement(Demon, Coven)}</p>
 * 
 * <p>Membrii de tip data sunt:</p>
 * <p>coven - cuibul din care se vor retrage demoni</p>
 * <p>warStatus - variabila folosita pentru a opri threadurile</p>
 * <p>isAccess - variabila booleana care determina atunci cand un demon acceseaza firul de pensionare</p>
 * <p>semaphoreDemons - instanta a clasei Semaphore care sincronizeaza accesul la fir a threadurilor</p>
 * 
 * @author Marinescu Ana
 *
 */
public class Retirement extends Thread{
	
	private Coven coven;
	private War warStatus;
	private boolean isAccess=false;
	private Semaphore semaphoreDemons=new Semaphore(0);
	
	/**
	 * Constructorul clasei
	 * @param covens cuibul din care urmeaza sa retraga demoni
	 * @param war - instanta folosita pentru a opri normal threadul
	 */
	public Retirement(Coven covens, War war) {
		
		this.coven=covens;
		this.warStatus=war;
		
	}
	
	/**
	 * Functia care descrie functionalitatea threadului: threadul extrage demoni la fiecare x milisecunde, unde x este un numar aleator ce apartine intervalului [500-800]
	 */
	@Override
	public void run() {
		
		Random randomObject=new Random();
		
		while (warStatus.WarIsEnd()==false) {
			
			try {
				
				Thread.sleep(randomObject.nextInt(301)+500);
				
				coven.SendDemonToRetirement(null);
			
			} catch (Exception e) {
				
				e.printStackTrace();
			
			}
		
		}
	
	}
	
	/**
	 *  Metoda este apelata de demoni atunci cand acesta vrea sa iasa la pensie. Daca un thread a accesat metoda, se asteapta pana cand i se ofera permisii,
	 *  apoi apeleaza functia   SendDemonToRetirement pentru a retrage demonul din cuibul din care face parte.
	 * @param demon demonul care vrea sa iasa la pensie
	 * @param covenDemon cuibul din care face parte demonul
	 */
	public void RequestRetirement(Demon demon, Coven covenDemon)
	{
		while (isAccess==true) {
			
			try {
				
				semaphoreDemons.tryAcquire();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		isAccess=true;
		
		covenDemon.SendDemonToRetirement(demon);
		
		isAccess=false;
		
		semaphoreDemons.release();
	}
}
