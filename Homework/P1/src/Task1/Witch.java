package Task1;

import java.util.Random;

/**
 * <p>Witch este un thread, care creaza portiuni. Fiecare vrajitoare are cate doua portiuni de creat, si pentru ambele are lista unica de ingrediente necesare. Cele 10 vrajitoare se stiu de la inceput.</p>
 * <p>Metodele necesare sunt urmatoarele:</p>
 * <p>{@link #Witch(int,int, int[], int[], Coven[], int, MagicPortal)} constructorul clasei</p>
 * <p>{@link #run()}</p>
 * <p>{@link #RequestPotions()}</p>
 * <p>{@link #LoseIngredients()}</p>
 * <p>{@link #SentPotions()}</p>
 * <p>{@link #GoToGetIngredientsPotion(int[])}</p>
 * <p>{@link #CreatePotion(int[])}</p>
 * 
 * <p>Datele membre ale clasei sunt:</p>
 * <p>magicBag[] - geanta magica in care vrajitoarea isi tine portiunile luate din cuiburi</p>
 * <p>idPotion1 - idul primei portiuni</p>
 * <p>idPotion2- idul celei de-a doua portiuni</p>
 * <p>ingredientsPotion1[] - lista de ingrediente necesare pentru a creea prima portiune</p>
 * <p>ingredientsPotion2[] - lista de ingrediente necesare pentru a creea a doua portiune</p>
 * <p>listOfCovens[] - lista de cuiburi disponibile in cercul vrajitorului</p>
 * <p>nCovens - numarul de cuiburi</p>
 * <p>magicPortal - portalul magic prin care comunica cu marele vrajitor pentru schimbul de portiuni</p>
 * <p>numberPotion1Created - numarul de portiuni cu idul idPotion1 create</p>
 * <p>numberPotion2Created - numarul de portiuni cu idul idPotion2 create</p>
 * @author Marinescu Ana
 *
 */
public class Witch extends Thread{

	public int[] magicBag;
	private int idPotion1;
	private int idPotion2;
	private int[] ingredientsPotion1;
	private int[] ingredientsPotion2;
	private Coven[] listOfCovens;
	private int nCovens;
	private MagicPortal magicPortal;
	private int numberPotion1Created=0;
	private int numberPotion2Created=0;
	
	/**
	 * Constructorul clasei care initializeaza variabilele cu acelasi nume, si initializeaza vectorul de frecventa magicBag cu 0.
	 * @param idPotion1 idul primei portiuni
	 * @param idPotion2 idul celei de-a doua portiuni
	 * @param ingredientsPotion1 lista de ingrediente necesare pentru a creea prima portiune
	 * @param ingredientsPotion2 lista de ingrediente necesare pentru a creea a doua portiune
	 * @param listOfCovens lista de cuiburi disponibile in cercul vrajitorului
	 * @param nCovens  numarul de cuiburi
	 * @param magicPortal portalul magic prin care comunica cu marele vrajitor pentru schimbul de portiuni
	 */
	public Witch(int idPotion1,
			int idPotion2,
			int[] ingredientsPotion1,
			int[] ingredientsPotion2,
			Coven[] listOfCovens,
			int nCovens,
			MagicPortal magicPortal) {
		
		magicBag=new int[10];
		for(int iterator=0;iterator<10;iterator++)
			magicBag[iterator]=0;
		
		this.idPotion1=idPotion1;
		this.idPotion2=idPotion2;
		this.ingredientsPotion1=ingredientsPotion1;
		this.ingredientsPotion2=ingredientsPotion2;
		this.listOfCovens=listOfCovens;
		this.nCovens=nCovens;
		this.magicPortal=magicPortal;

	}
	
	/**
	 * Fiecare vrajitoare trebuie sa mearga in cuiburi si sa ia ingrediente, apoi dupa un timp aleator cuprins intre 10-30 milisecunde creaza portiuni daca au toate ingredientele necesare
	 * dupa care le trimit marelui vrajitor prin portalul magic. Toate astea se repeta pana cand marele vrajitor are toate portiunile.
	 */
	@Override
	public void run() {
		
		Random object=new Random();
		
		while(magicPortal.SaveWorld()==false)
		{
			GoToGetIngredientsPotion(ingredientsPotion1);
			
			GoToGetIngredientsPotion(ingredientsPotion2);
			
			try {
				
				Thread.sleep(object.nextInt(21)+10);
				
				numberPotion1Created+=CreatePotion(ingredientsPotion1);
				
				Thread.sleep(object.nextInt(21)+10);
				
				numberPotion2Created+=CreatePotion(ingredientsPotion2);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			SentPotions();
		}
	}
	
	/**
	 * In cazul in care se intalneste cu un strigoi, aceasta cere portiuni de la marele vrajitor folosind metoda aceasta, 
	 * care apeleaza metoda RequestPotions() din clasa MagicPortal(detalii acolo).
	 * @return true daca portiunile au fost trimise sau false in caz contrar
	 */
	public boolean RequestPotions()
	{
		return magicPortal.RequestPotions();
	}
	
	/**
	 * In cazul in care se intalneste cu un strigoi, aceasta cere portiuni de la marele vrajitor. Daca marele vrajitor nu 
	 * are portiuni disponibile, aceasta isi pierde o parte din ingredientele pe care le are.
	 */
	public void LoseIngredients()
	{
		for(int ingredients:magicBag)
		{
			if(ingredients>0)
			{
				ingredients--;
			}
		}
	}
	
	/**
	 * Cand vrajitoarea creaza portiuni, trebuie sa le trimia marelui vrajitor. 
	 */
	private void SentPotions()
	{
		if(numberPotion1Created>0)
			{
				magicPortal.SendPotion(idPotion1,numberPotion1Created);
				numberPotion1Created=0;
			}
		if(numberPotion2Created>0)
		{
			magicPortal.SendPotion(idPotion2,numberPotion2Created);
			numberPotion2Created=0;
		}
	}
	
	/**
	 * Functia este folosita pentru a vizita cuiburi. Cat timp vrajitoarea nu gaseste niciun cuib care sa aiba ingredientele din lista primita ca parametru,
	 * se va genera aleator idul cuibului care trebuie vizitat pana cand gaseste ingrediente.
	 * @param ingredientsList lista de ingrediente necesare pentru a crea portiuni
	 */
	private void GoToGetIngredientsPotion(int[] ingredientsList)
	{
		
		Random objectRandom=new Random();
		
		int covenChoose;
		
		do {
		
			covenChoose=objectRandom.nextInt(nCovens);
		
		}while(listOfCovens[covenChoose].TakeIngredients(ingredientsList, magicBag,this)==-1);
	
	}
	
	/**
	 * Metoda este folosita pentru a crea portiuni astfel: avem lista cu ingredientele necesare pentru o portiune si saculetul cu ingrediente. Se parcurg ingredientele din sac si se gaseste ingredientul cu cea mai mica frecventa de aparitie
	 * ce se atribuie variabilei min. Daca unul din ingrediente lipseste atunci nu se poate crea portiunea. Daca insa se gaseste un minim, se parcurge din nou magicBag pentru a decrementa cu min
	 * ingredientele luate pentru a crea portiunea.
	 * @param ingredientsList lista ingredientelor necesare pentru a crea portiunea
	 * @return 0 daca nu s-a putut crea portiunea, sau un numar natural mai nenul care reprezinta numarul de portiuni create 
	 */
	private int CreatePotion(int[] ingredientsList)
	{
		boolean create=true;
		
		int min=magicBag[ingredientsList[0]];
		
		for(int ingredient:ingredientsList)
		{
			if(magicBag[ingredient]==0)
				{
					return 0;
				}
			
			if(magicBag[ingredient]<min)
				{
					min=magicBag[ingredient];
				}
		}
		
		if(create==true)
		{
			for(int ingredient:ingredientsList)
			{
				magicBag[ingredient]-=min;
			}
			
		}
		
		return min;
	}

	
}
