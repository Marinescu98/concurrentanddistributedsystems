package Task1;

import java.util.Random;

/**
 * Undead este un thread care contine urmatoarele metode:
 * <p>{@link #Undead(int, Coven[], War) constructorul clasei}</p>
 * <p>{@link #run()}</p>
 * <p>Membrii de tip data sunt:</p>
 * <p>NCOVENS - numarul de cuiburi din cerc</p>
 * <p>covens[] - cuiburile din cercul vrajitorului</p>
 * <p>warStatus - instanta a clasei War folosita pentru a opri threadurile normal atunci cand vrajitorul are toate portiunile pentru a salva lumea</p>
 * <p>randomObject - instanta a clasei Random folosita pentru a genera numere aleator</p>
 *
 *	@author Marinescu Ana
 */
public class Undead extends Thread{
	
	private int NCOVENS;
	private Coven[] covens;
	private War warStatus;
	private Random randomObject;
	
	/**
	 * Constructorul care initializeaza variabilele cu acelas nume:
	 * @param NCOVENS numarul de cuiburi din cerc
	 * @param covens cuiburile din cercul vrajitorului
	 * @param war instanta a clasei War folosita pentru a opri threadurile normal atunci cand vrajitorul are toate portiunile pentru a salva lumea
	 */
	public Undead(int NCOVENS, Coven[] covens, War war) {
		this.covens=covens;
		this.warStatus=war;
		this.NCOVENS=NCOVENS;
		this.randomObject=new Random();
	}
	
	/**
	 * Strigoii ataca cuibul dupa un timp aleator ales din intervalul [500,1000]. Cat timp vrajitorul nu are portiuni pentru a castiga, se alege un cuib din cele NCOVENS,
	 * si se ataca cuibul apeland metoda AttackCoven() din clasa Coven(detalii acolo).
	 */
	@Override
	public void run() {
		
		int covenChoose;
		
		while(warStatus.WarIsEnd()==false)
		{
			try {
				
				Thread.sleep(randomObject.nextInt(501)+500);
				
				covenChoose=randomObject.nextInt(NCOVENS);
				
				if(covenChoose<NCOVENS && covenChoose>=0)
				{
					covens[covenChoose].AttackCoven();
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
