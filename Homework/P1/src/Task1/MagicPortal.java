package Task1;

import java.util.Random;

/**
 * Clasa implementeaza un canal ce comunica cu marele vrajitor atunci cand o vrajitoare trimite portiuni marelui vrajitor, cand threadurile vor sa afle daca marele vrajitor are toate portiunile,
 * si pentru a primii protiuni de la marele vrajitor atunci cand se lupta cu un strigoi. Metodele clasei sunt:
 * <p>{@link #MagicPortal()} constructorul clasei</p>
 * <p>{@link #SendPotion(int,int)}</p>
 * <p>{@link #RequestPotions() }</p>
 * <p>{@link #SaveWorld()}</p>
 * <p>Membrii de tip data sunt:</p>
 * <p>potionsName[] - numele celor 20 de portiuni</p>
 * <p>potions[] - portiunile trimise de vrajitoare marelui vrajitor </p>
 * @author Marinescu Ana
 *
 */
public class MagicPortal {

	private static final String[] potionsName= {
			"venom",
			"gray potion",
			"green potion",
			"magic potion",
			"iron potion",
			"portion of fire",
			"wooden potion",
			"blue potion",
			"wild potion",
			"raw potion",
			"old potion",
			"young potion",
			"pain",
			"tears",
			"water",
			"dead potion",
			"empty potion",
			"broken potion",
			"dirty potion",
			"clean potion"
	}; 
	private int[] potions;
	
	/**
	 * Constructorul clasei care initializeaza vectorul de frecvente ce contine portiunile trimise, cu 0.
	 */
	public MagicPortal() {
		potions=new int[20];
		
		for(int iterator=0;iterator<20;iterator++)
			potions[iterator]=0;
	}
	
	/**
	 * Atunci cand o vrajitoare trimite portiuni, se apeleaza aceasta metoda care incrementeaza numarul de portiuni de tipul idPortion cu numberOfPortions
	 * @param idPotion id-ul portiunii create si trimise de vrajitoare
	 * @param numberOfPotions numarul de portiuni de tip isPortion trimise
	 */
	public void SendPotion(int idPotion,int numberOfPotions)
	{
		potions[idPotion]+=numberOfPotions;
		
		System.out.println(numberOfPotions+" "+potionsName[idPotion]+" were sent.");
	}
	
	/**
	 * Cand o vrajitoare se lupta cu un strigoi, aceasta cere marelui vrajitor un numar de portiuni aleator, cuprins intre intervalul [2,5], si trimite din fiecare portiune,
	 * daca are suficiente, ceea ce inseamna ca se decrementeaza numarul total de portiuni trimise din fiecare portiune disponibila cu noPortions.
	 * @return true daca vrajitoarea primeste portiunile, false daca nu
	 */
	public boolean RequestPotions()
	{
		Random randomObject=new Random();
		
		int noPotions=randomObject.nextInt(4)+2;
		
		for(int potion:potions)
		{
			if(potion>=noPotions)
			{
				potion-=noPotions;
				
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Functia verifica daca marele vrajitor a primit cate o portiune din toate cele 20 disponibile.
	 * @return true daca marele vrajitor are cate o portiune din fiecare, false daca nu
	 */
	public boolean SaveWorld()
	{
		boolean saveWorld=true;

		for(int portion:potions)
		{
			if(portion==0)
				{
					saveWorld=false;
				}
				
		}

		return saveWorld;
	}
}
