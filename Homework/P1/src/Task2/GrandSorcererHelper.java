package Task2;

import java.util.Random;

/**
 * Threadul de fata implementeaza ajutorul marelui vrajitor, care are rolul de a adauga demoni in cuib. Pentru asta, metodele folosite sunt:
 * <p>{@link #GrandSorcererHelper(int, Coven[],War)} constructorul clasei</p>
 * <p>{@link #run()}</p>
 * <p>{@link #CreateDemon()}</p>
 * <p>Membrii de tip data sunt:</p>
 * <p>objectRandom - variabila folosita pentru a genera random numere</p>
 * <p>numberOfCovens - numarul de cuiburi din cercul vrajitorului</p>
 * <p>covens[] - cuiburile din cercul vrajitorului</p>
 * <p>demonCreated - numarul de demoni creati care va deveni si idul demonului</p>
 * <p>warStatus - instanta clasei War folosita pentru a opri executia threadului normal, atunci cand vrajitorul are toate portiunile si razboiul se termina</p>
 * @author Marinescu Ana
 *
 */
public class GrandSorcererHelper extends Thread{

	private Random objectRandom;//variabila folosita pentru a genera random numere
	private int numberOfCovens;//numarul de cuiburi din cercul vrajitorului
	private Coven covens[];//cuiburile din cercul vrajitorului
	private int demonCreated=0;//numarul de demoni creati
	private static War warStatus;
	
	/**
	 * Constructorul clasei care initialiteaza variabilele cu acelasi nume:
	 * @param numberOfCovens numarul de cuiburi din cercul vrajitorului
	 * @param covens cuiburile din cercul vrajitorului
	 * @param war instanta clasei War folosita pentru a opri executia threadului normal, atunci cand vrajitorul are toate portiunile si razboiul se termina
	 */
	public GrandSorcererHelper(int numberOfCovens, Coven covens[],War war) {	
		this.covens=covens;
		this.numberOfCovens=numberOfCovens;
		this.warStatus=war;
		objectRandom=new Random();
	}
	
	/**
	 * Metoda run implementeaza rolul threadului, adica de a crea ingrediente la fiecare x milisecunde, unde x este un numar aleator ales din intervalul [500,1000].
	 * Odata ce timpul s-a scurs, se apeleaza metoda {@link #CreateDemon()}.
	 */
	@Override
	public void run() {
		
		while(warStatus.WarIsEnd()==false)
		{
			try {
			
				Thread.sleep(objectRandom.nextInt(501)+500);
			
				CreateDemon();			
			
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Ajutorul vrajitorului trebuie sa creeze demoni. Astfel, initial demonul este creat, variabila care contorizeaza numarul de demoni creati este incrementata,
	 * se alege un cuib din lista de cuiburi, si cat timp cuibul nu are locuri disponibile pentru demon, se cauta un cuib nou. Odata gasit, demonul este "trimis" acolo.
	 */
	public void CreateDemon()
	{	
		Demon demon=null;
		
		demonCreated++;
		
		int noCoven=objectRandom.nextInt(numberOfCovens);
		
		while(covens[noCoven].AddDemon(demon,demonCreated)==false)
			noCoven=objectRandom.nextInt(numberOfCovens);
		
	}
}
