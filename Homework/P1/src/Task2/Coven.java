package Task2;

import java.util.*;
import java.util.concurrent.Semaphore;

/**
 * Clasa este un thread care implemteaza urmatoarele metode:
 * <p>{@link #Coven(int, War)} constructorul clasei</p>
 * <p>{@link #GetMatrixSize()}</p>
 * <p>{@link #run()}</p>
 * <p>{@link #AttackCoven()}</p>
 * <p>{@link #TakeIngredients(int[], int[], Witch)}</p>
 * <p>{@link #AddIngredients(Demon)}</p>
 * <p>{@link #AddDemon(Demon, int)}</p>
 * <p>{@link #ScaresTheDemons()}</p>
 * <p>{@link #GetBarrier()}</p>
 * <p>{@link #GetSizeDemonsList()}</p>
 * 
 * Membrii data ai clasei sunt:
 * <p>cavenLocations[][] - cuiburile sunt matrici de dimensiune NLOCATIONS X NLOCATIONS, dar pentru sincronizare consideram locatiile instantele ale clasei Location</p>
 * <p>NLOCATIONS - dimensiunea matricii</p>
 * <p>idCoven - id-ul cuibului</p>
 * <p>demonsList - lista demonilor din cuib</p>
 * <p>NMAXDEMONS - numarul maxim de demoni pe care ii poate avea cuibul</p>
 * <p>ingredientsName[] - lista ingredientelor cu numele lor</p>
 * <p>chest - cufarul in care sunt tinute ingredientele</p>
 * <p>witch - vrajitoarea care acceseaza cubul pentru a luat ingrediente</p>
 * <p>warStatus - instanta a clasei War folosita pentru a opri threadurile normal atunci cand vrajitorul are toate portiunile pentru a salva lumea</p>
 * <p>semaphoreGrandSorcererHelper - semafor pentru a sincroniza accesul la cuib al ajutorului vrajitorului</p>
 * <p>semaphoreUndead - semafor pentru a sincroniza vizitele strigoilor</p>
 * <p>helperInCoven - variabila booleana care determina daca cuibul e accesat de ajutorul vrajitorului</p>
 * <p>undeadInCoven - variabila booleana care determina daca cuibul este accesat de strigoi</p>
 * <p>randomObject - instanta a clasei Random folosita pentru a genera numere aleatoare</p>
 * <p> barrier - bariera la care trebuie sa se opreasca demonii</p>
 * @author Marinescu Ana
 *
 */
public class Coven extends Thread{
	public Location[][] cavenLocations;//locatiile cuibului
	private int NLOCATIONS;//dimensiunea matricei
	private int idCoven;//idul cuibului
	private List<Demon> demonsList;//lista cu demonii din cuib
	private int NMAXDEMONS;//numarul maxim de demoni care incap in cuib
	private static final String ingredientsName[]= {
				"fish scales",
				"dust from the coven",
				"bird pain",
				"teacher's skull",
				"moving water",
				"rotten wood",
				"butterfly wings",
				"unicorn horn",
				"the spit out of the undead",
				"owl beak"
		}; //lista ingredientelor cu numele lor
	private Chest chest;//cufarul in care sunt tinute ingredientele
	private Witch witch=null;//vrajitoarea care acceseaza cubul pentru a luat ingrediente
	private static War warStatus ;//instanta a clasei War folosita pentru a opri threadurile normal atunci cand vrajitorul are toate portiunile pentru a salva lumea
	private Semaphore semaphoreGrandSorcererHelper=new Semaphore(1);//semafor pentru a sincroniza accesul la cuib al ajutorului vrajitorului
	private Semaphore semaphoreUndead=new Semaphore(1);//semafor pentru a sincroniza vizitele strigoilor
	private boolean helperInCoven=false;// variabila booleana care determina daca cuibul e accesat de ajutorul vrajitorului
	private boolean undeadInCoven=false;//variabila booleana care determina daca cuibul este accesat de strigoi
	private Random randomObject=new Random();//instanta a clasei Random folosita pentru a genera numere aleatoare
	private Barrier barrier;
	
	/**
	 * Getter
	 * @return returneaza dimensiunea listei cu demonii din cuib
	 */
	 public int GetSizeDemonsList()
	 {
		 return demonsList.size();
	 }
	 
	 /**
	  * Getter
	  * @return returneaza instanta clasei Barrier din cuib
	  */
	public Barrier GetBarrier()
	{
		return barrier;
	}
	
	/**
	 * Getter
	 * @return retureneza dimensiunea matricei locatiilor
	 */
	public int GetMatrixSize()
	{
		return NLOCATIONS;
	}
	
	/**
	 * Constructorul in care sunt inializare variabilele idCoven si warStatus. Tot aici este creat si initializat cufarul unde vor fi puse ingredientele, este definita dimensiunea matricei care este un numar aleator
	 * din intervalul [100,500], numarul maxim de demoni pe care il poate avea cuibul. De asemenea, este definita si initializata matricea locatiilor si fiecare locatie in parte, precum si lista demonilor din cuib si sunt afisate  detalii despre cuib.
	 * @param idCoven id-ul cuibului
	 * @param war instanta a clasei War
	 */
	public Coven(int idCoven, War war)
	{
		this.idCoven=idCoven;
		this.chest=new Chest(idCoven,ingredientsName);
		this.warStatus=war;
		 
		NLOCATIONS = (int) randomObject.nextInt(401) + 100;
		NMAXDEMONS=(int)NLOCATIONS/2;
		
		cavenLocations=new Location[NLOCATIONS][NLOCATIONS];
		for(int iteratorX=0;iteratorX<NLOCATIONS;iteratorX++)
			for(int iteratorY=0;iteratorY<NLOCATIONS;iteratorY++)
				cavenLocations[iteratorX][iteratorY]=new Location();
		
		System.out.println("Coven "+idCoven+": ");
		System.out.println("NLOCATIONS = "+NLOCATIONS);
		System.out.println("NMAXDEMONS = "+NMAXDEMONS+"\n");

		demonsList=new ArrayList<Demon>();
		
		barrier=new Barrier(this);
	}

	/**
	 * Metoda implementeaza sarcinile pe care le are cuibul. Cat timp vrajitorul nu are toate portiunile, din 10secunde in 10secunde fiecare cuib isi sperie demonii, adica ii face sa isi piarda ingredientele. 
	 * Daca razboiul se incheie fiecare cuib este responsabil de oprirea demonilor prin apelare functiei  KillDemon() care determina oprirea normala a demonilor
	 */
	@Override
	public void run() {
		
		while(warStatus.WarIsEnd()==false)
		{
			try {
			
				Thread.sleep(10000);
			
				ScaresTheDemons();
			
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		for(Demon demon:demonsList)
		{
			demon.KillDemon();	
		}
	}
	
	/**
	 * La un interval de timp aleator strigoii ataca cuiburile, insa atacul cuiburilor si adaugarea demonilor de catre ajutorul vrajitorului trebuie sincronizate pentru a nu intalni probleme atunci 
	 * cand unii incearca sa scoata demoni din lista cu demoni a cuibului sau si altii sa adauge. Astfel, prima data se verifica daca nu cumva cuibul este accesat de ajutorul vrajitorului sau de un alt strigoi, si in cazul acesta se asteapta pana cand cuibul poate fi accesat
	 * de catre strigoi. Odata ce poate fi accesat, variabila booleana undeadInCoven se face true,
	 * semn ca un strigoi a accesat cuibul. Daca in cuib nu se afla nicio vrajitoare, atunci un numar aleator de demoni este retras (numar cuprins intre intervalul [5,10]), cuibul isi pierde toate ingredientele din cufar si ceilalti demoni ramasi in cuib 
	 * sunt speriati. Daca insa exista o vrajitoare in cuib la momentul atacului, aceasta se lupta cu el. Pentru a se lupta cu strigoiul, vrajitoarea cere de la marele vrajitor portiuni. Daca le primeste, strigoiul este speriat, cuibul nu isi pierde demonii sau 
	 * ingredientele iar demonii nu vor fi speriati, iar daca nu primeste nimic, atunci ea pierde o parte din ingredientele pe care le are asupra ei. La final, undeadInCoven devine false si se apeleaza release(), semn ca strigoiul a parasit cuibul si acesta poate
	 * fi accesat de celelalalt thread, GrandSorcererHelper. 
	 */
	public void AttackCoven()
	{
		while(helperInCoven==true)
		{
			try {
				
				semaphoreGrandSorcererHelper.acquire();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		while(undeadInCoven==true)
		{
			try {
				
				semaphoreUndead.acquire();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		undeadInCoven=true;
		
		if(witch==null)
		{
			int noDemons=randomObject.nextInt(6)+5;
			
			for(int iterator=0;iterator<noDemons && demonsList.size()>0;iterator++)
			{
				demonsList.get(0).KillDemon();
				demonsList.remove(0);
			}
			
			chest.LoseIngredients();
			
			ScaresTheDemons();
			
			System.out.println("An undead attacked the coven "+idCoven+": "+noDemons+" demons were removed and the ingredients were lost.");
		}
		
		if(witch!=null)
		{
			if(witch.RequestPotions()==true)
			{
				System.out.println("An undead attacked the coven "+idCoven+" but a witch scared him.");
			
			}else {
			
				if(witch!=null)
				{
					witch.LoseIngredients();
				}
				
				System.out.println("An undead attacked the coven "+idCoven+" and a witch lost ingredients in the fight.");
			}
					
		}
		
		undeadInCoven=false;
		
		semaphoreUndead.release();
	}
	
	/**
	 * Vrajitoarele viziteaza cuiburile pentru a lua ingredientele necesare pentru portiuni. Pentru asta se apeleaza metoda curenta astfel: cand vrajitoarea acceseaza cuibul
	 * instanta witch este initializata. Se apeleaza metoda takeIngredients din Chest pentru a extrage cate un ingredient din cele de care are nevoie vrajitoarea(daca sunt),
	 * numarul de ingrediente extrase este atribuit variabilei locale noOfIngredientsTaken, instanta witch devina nula si noOfIngredientsTaken este returnat.
	 * @param ingredientsList lista de ingrediente necesare vrajitoarei pentru a crea portiuni
	 * @param magicBag geanta in care isi pune ingredientele(vector de frecvente de dimensiunea 10)
	 * @param witchGuest instanta a vrajitoarei care acceseaza cuibul pentru a lua ingrediente
	 * @return -1 daca niciun ingredient nu a fost retras, un numar natural care indica cate ingrediente din cele 10 disponibile au fost retrase
	 */
	public int TakeIngredients(int[] ingredientsList, int[] magicBag, Witch witchGuest) {
		
		witch=witchGuest;
		
		int noOfIngredientsTaken = chest.takeIngredients(ingredientsList,witchGuest);
		
		witch=null;
		
		return noOfIngredientsTaken;
	}
	
	/**
	 * Un demon creaza ingrediente la fiecare miscare, iar pentru asta apeleaza metoda de fata care adauga in cufar ingrediente
	 * @param demon demonul care a creat ingredientele
	 */
	public void AddIngredients(Demon demon)
	{	
		chest.addIngredient(demon);
	}
	
	/**
	 * GrandSorcererHepler este ajutorul vrajitorului care adauga in cuiburi demoni. Pentru asta se apeleaza metoda de fata. Insa, ajutorul vrajitorului nu poate adauga in timp ce un strigoi scoate alti demoni din lista, asa ca initial, daca un
	 * strigoi acceseaza cuibul, se asteapta pana cand termina, apeland metoda acquire a semaforului corespunzator strigoiului. Odata ce threadul a fost eliberat, helperInCoven devine true, semn ca ajutorul vrajitorului a acaparat threadul.
	 * Daca lista demonilor este plina, atunci se returneaza false, in caz contrar se gaseste un loc in matricea pozitiilor in care demonul sa poata fi inserat. Pentru asta se folosesc doua variabile locale care indica linia si respectiv coloana. Se itereaza prin matrice pana 
	 * se gaseste un loc disponibil pentru demon. Odata ce locul este gasit se ocupa prin apelarea metodei Occupy, demonul este initializat, adaugat in lista de demoni a cuibului si pornit. La final, variabila helperInCoven devine false si ajutorul vrajitorului elibereaza threadul.
	 * @param demon - demonul care trebuie adaugat in lista
	 * @param idNewDemon - idul noului demon
	 * @return returneaza false daca lista e plina si demonul nu poate fi adaugat si true in cazul in care este adaugat
	 */
	public boolean AddDemon(Demon demon, int idNewDemon)
	{
		while(undeadInCoven==true)
		{
			try {
				
				semaphoreUndead.acquire();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		helperInCoven=true;
		
		if(demonsList.size()== NMAXDEMONS-1)
			{
				helperInCoven=false;
				
				semaphoreGrandSorcererHelper.release();
			
				return false;
			}
		
		int positionX=0, positionY=0;
		
		boolean findPlace=false;
		
		for(int iteratorX=0;iteratorX<NLOCATIONS && findPlace==false;iteratorX++)
			{
				for(int iteratorY=0;iteratorY<NLOCATIONS && findPlace==false;iteratorY++)
				{
					if(cavenLocations[iteratorX][iteratorY].isBusy==false)
					{
						positionX=iteratorX;
						positionY=iteratorY;
						findPlace=true;
					}
				}
			}
		
		demon=new Demon(idNewDemon,positionX,positionY,this);
		
		cavenLocations[positionX][positionY].Occupy(demon);
		
		demonsList.add(demon);
		
		demon.start();
		
		System.out.println("Demon "+idNewDemon+" was registered in coven "+idCoven);
		
		helperInCoven=false;
		
		semaphoreGrandSorcererHelper.release();
		
		return true;
		
	}
	
	/**
	 * Getter
	 * @return returneaza instanta clasei War 
	 */
	public War GetWarStatus()
	{
		return warStatus;
	}
	
	/**
	 * La fiecare 10 secunde cuibul isi anunta demonii sa se opreasca pentru o secunda. Pentru asta, daca lista nu este goala, se itereaza prin ea si se apeleaza pentru
	 * fiecare demon metoda ce pune demonul in asteptare, DemonWait, explicata deja la clasa Demon. 
	 */
	private void ScaresTheDemons() {

		if(demonsList.size()>0)
		{
			for (Demon demon : demonsList) {		
			
				demon.ScaredDemon();		
			
			}	
		}

	}
}
