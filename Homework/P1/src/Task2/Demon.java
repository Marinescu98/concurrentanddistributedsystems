package Task2;

import java.util.Random;

/**
 * <p>Demon este un thread, care creaza toate ingredientele pentru portiuni. Ei sunt creati aleatoriu de catre ajutorul vrajitorului.</p>
 * <p>Metodele necesare sunt urmatoarele:</p>
 * <p>{@link #Demon(int, int, int, Coven)}</p>
 * <p>{@link #run()}</p>
 * <p>{@link #LoseNoIngredients()}</p>
 * <p>{@link #KillDemon()}</p>
 * <p>{@link #DemonWait (int, int)}</p>
 * <p>{@link #GetNoIngredients()}</p>
 * <p>{@link #CreateIngredient()}</p>
 * <p>{@link #MeetDemon()}</p>
 * <p>{@link #HitWall()}</p>
 * <p>{@link #GetId()}</p>
 * <p>{@link #MoveRight()}</p>
 * <p>{@link #MoveLeft()}</p>
 * <p>{@link #MoveUp()}</p>
 * <p>{@link #MoveDown()}</p>
 * <p>{@link #ScaredDemon()}</p>
 * <p>{@link #SetIsSleep(boolean)}</p>
 *
 * <p>Datele membre ale clasei sunt:</p>
 * <p>idDemon - id-ul demonului</p>
 * <p>positionX - variabila care indica pe ce linie se afla demonul in cuib</p>
 * <p>positionY- indica pe ce coloana se afla demonul in cuib</p>
 * <p>socialSkills - abilitatile lui sociale</p>
 * <p>wallsHits - numarul de lovituri in ziduri</p>
 * <p>noIngredients - numarul de ingrediente pe care le poate creea la o miscare; maximul de ingrediente pe care le poate creea este 10</p>
 * <p>isAlive - variabila booleana folosita pentru a opri normal executia threadului, fara apelarea metodei join()</p>
 * <p>coven - instanta a clasei Coven care indica cuibul din care face parte demonul</p>
 * <p>isSleep - boolean care pentru true inseamna ca demonul se afla in bariera, iar la false inseamna ca a parasit bariera</p>
 * @author Marinescu Ana
 *
 */
public class Demon extends Thread{
	
	private int idDemon;//id-ul demonului
	private int positionX;//variabila care indica pe ce linie se afla demonul in cuib
	private int positionY;//indica pe ce coloana se afla demonul in cuib	
	private int socialSkills=0;//abilitatile sociale
	private int wallsHits=0;//numarul de lovituri in ziduri
	private int noIngredients=1;//	numarul de ingrediente pe care le poate creea la o miscare
	private boolean isAlive=true;//variabila booleana folosita pentru a opri normal executia threadului, fara apelarea metodei join()
	private  Coven coven;//instanta a clasei Coven care indica cuibul din care face parte demonul
	private boolean isSleep=false;
	/**
	 * Constructorul clasei care initializeaza membrii clasei cu acelasi nume 
	 * @param idDemon  id-ul demonului
	 * @param positionX  variabila care indica pe ce linie se afla demonul in cuib
	 * @param positionY  indica pe ce coloana se afla demonul in cuib
	 * @param coven  instanta a clasei Coven care indica cuibul din care face parte demonul
	 */
	public Demon(int idDemon, int positionX, int positionY, Coven coven){	 
		this.coven=coven;
		this.idDemon=idDemon;
		this.positionX=positionX;
		this.positionY=positionY;
	}
	 
	/**
	 * Functia run defineste rolul firului. Demonul este responsabil de crearea ingredientelor, si atata timp cat este in viata adica variabila isAlive e true sau atata timp cat confruntarea nu s-a terminat, acesta ruleaza.
	 * Daca nu se afla la bariera, verifica daca poate merge la dreapta, daca nu la stanga, jos sau sus. Daca una din miscari se poate executa, se apeleaza metoda corespunzatoare ei. Dupa ce ingredientul 
	 * a fost creat, demonul se odihneste timp de 30 milisecunde. In cazul in care nici una din mutari nu este posibila, atunci inseamna ca demonul este incojurat de alti demoni
	 * astfel astepta timp de x milisecunde, unde x este o valoare aleatoare din intervalul 10-50. 
	 */
	 @Override
	public void run() {
		
		 while(isAlive==true && coven.GetWarStatus().WarIsEnd()==false)
		 {
			 if(positionX==positionY && isSleep==false)
			{
				 coven.GetBarrier().StopBarrier(this);
			}
			 
			 if(isSleep==false)
			 {  
				 if(MoveRight()==false)
				 {
					 if(MoveLeft()==false)
					 {
						 if(MoveDown()==false)
						 {
							 if(MoveUp()==false)
							 {
								 DemonWait(10, 50);
							 }
							 
						 }
					 }
				 }
		
				 if(isSleep==false)
				 {
					 DemonWait(-1, 30);
				 }
			 }		
		 }
		 
	}
	 

	 /**
	  * Setter
	  * @param set boolean care pentru true inseamna ca demonul se afla in bariera, iar la false inseamna ca a parasit bariera 
	  */
	 public void SetIsSleep(boolean set)
	 {
		 isSleep=set;
	 }
	
	/**
	 * Atunci cand un demon este speriat, acesta se opreste din lucru pentru o secunda si isi pierde capacitatea de a pierde mai multe ingrediente.
	 */
	 public void ScaredDemon()
	 {
		DemonWait(-1,100);			
			
		LoseNoIngredients();	
	 }
	 
	 /**
	  * Atunci cand demonii sunt speriati, acestia isi pierd ingredientele. Cum toate ingredientele sunt puse separat intr-un "cufar" si nu sunt 
	  * pastrate asupra lor, in loc de ingrediente demonii isi pierd capacitatea de a crea mai multe ingredientele la o miscare.
	  */
	public void LoseNoIngredients()
	{
		noIngredients=1;
	}
	 
	/**
	 * Metoda ofera posibilitatea de a opri normal un demon, omorandu-l, adica facand variabila booleana isAlive, false.
	 */
	public void KillDemon()
	{
		isAlive=false;
	}
	
	/**
	 * In aceasta metoda este apelata metoda sleep care primeste limitele unui interval. Daca limita minima e -1, atunci se va apela sleep pentru maxMiliseconds milisecunde,
	 * daca este un numar natural, atunci se alege un numar din intervalul [minMiliseconds,maxMiliseconds]
	 * @param minMiliseconds  limita inferioara a intervalului
	 * @param maxMiliseconds  limita superiaora a intervalului
	 */
	private void DemonWait (int minMiliseconds, int maxMiliseconds)
	{
		Random objectRandom=new Random();
		
		try {
			
			if(minMiliseconds==-1)
			{
				Thread.sleep(maxMiliseconds);
			}
			
			if(minMiliseconds>=0)
			{
				Thread.sleep((objectRandom.nextInt(maxMiliseconds-minMiliseconds+1))+minMiliseconds);
			}
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Getter pentru a returna numarul de ingrediente pe care un demon il poate creea la o miscare.
	 * @return numarul de ingrediente pe care un demon il poate creea la o miscare.
	 */
	public int GetNoIngredients()
	{
		return noIngredients;
	}
	
	/**
	 * Metoda apeleaza AddIngredients() din clasa coven (sunt detalii acolo)
	 */
	public void CreateIngredient()
	{
		coven.AddIngredients(this);
	}

	/**
	 * Atunci cand un demon intalneste alt demon, ii cresc abilitatile sociale. La 100 de abilitati sociale, creste posibilitatea de a crea mai multe ingrediente la o miscare, dar nu mai multe de 10.
	 * Astfel ca, odata ce intalneste un demon, abilitatile sociale ii cresc cu 50 de puncte.
	 */
	public void MeetDemon()
	{		
		socialSkills+=50;
			
		if(socialSkills==100)
		{
			if(noIngredients<=10)
				{
					noIngredients++;
				}
		}
	}
	 
	/**
	 * Atunci cand un demon loveste un zid, variabila raspunzatoare de contorizarea acestora creste. La 10 lovituri de zid le vor fi luate 100 de puncte de abilitati sociale.
	 */
	private void HitWall()
	{		
		wallsHits++;
			
		if(wallsHits==10)
		{
			socialSkills-=100;
			wallsHits=0;
		}
	}
	
	/**
	 * Getter
	 * @return returneaza id-ul demonului
	 */
	public int GetId()
	{
		return idDemon;
	}
	
	/**
	 * Metoda implementeaza deplasarea demonului la dreapta, adica este incrementata variabila care indica pe ce coloana se afla demonul. In cazul in care in dreapta lui este un perete, se apeleaza metoda {@link HitWall()}
	 * adica se considera ca demonul a lovit un perete si se returneaza false. Daca aceea pozitie este ocupata de alt demon, de asemenea se returneaza false. Daca insa nici una din variantele prezentate nu este valida, atunci
	 * demonul se poate deplasa la dreapta. Deplasarea se face prin eliberarea pozitiei curente a demonului, adica prin apelarea metodei Move() din clasa Location (mai multe detalii acolo),
	 * si incercarea de a ocupa positia din dreapta prin apelarea metodei Occupy tot a clasei Location. In caz ca nici asta nu merge, se retuneaza false.
	 * @return false daca demonul nu se poate deplasa la dreapta, true in caz contrar
	 */
	private boolean MoveRight()
	{
		if(positionY==coven.GetMatrixSize()-1)
		{	
			HitWall();			
			return false;
		}
		
		if(coven.cavenLocations[positionX][positionY+1].isBusy)
			{	
				return false;
			}
		
		coven.cavenLocations[positionX][positionY].Move();
		
		if(coven.cavenLocations[positionX][positionY+1].Occupy(this))
		{
			positionY++;
			
			return true;
		}
					
		return false;
	}
	
	/**
	 * Metoda implementeaza deplasarea demonului la stanga, adica este decrementata variabila care indica pe ce coloana se afla demonul. In cazul in care in stanga lui este un perete, se apeleaza metoda {@link HitWall()}
	 * adica se considera ca demonul a lovit un perete si se returneaza false. Daca aceea pozitie este ocupata de alt demon, de asemenea se returneaza false. Daca insa nici una din variantele prezentate nu este valida, atunci
	 * demonul se poate deplasa la stanga. Deplasarea se face prin eliberarea pozitiei curente a demonului, adica prin apelarea metodei Move() din clasa Location (mai multe detalii acolo),
	 * si incercarea de a ocupa pozitia din stanga prin apelarea metodei Occupy tot a clasei Location. In caz ca nici asta nu merge, se retuneaza false.
	 * @return false daca demonul nu se poate deplasa la stanga, true in caz contrar
	 */
	private boolean MoveLeft()
	{
		if(positionY==0)
		{	
			HitWall();			
			return false;
		}
			
		if(coven.cavenLocations[positionX][positionY-1].isBusy)
			{
				return false;
			}
		
		coven.cavenLocations[positionX][positionY].Move();
		
		if(coven.cavenLocations[positionX][positionY-1].Occupy(this))
		{
			positionY--;
			
			return true;
		}
				
		return false;
		
	}
	
	/**
	 * Metoda implementeaza deplasarea demonului in sus, adica este decrementata variabila care indica pe ce linie se afla demonul. In cazul in care este un perete, se apeleaza metoda {@link HitWall()}
	 * adica se considera ca demonul a lovit un perete si se returneaza false. Daca aceea pozitie este ocupata de alt demon, de asemenea se returneaza false. Daca insa nici una din variantele prezentate nu este valida, atunci
	 * demonul se poate deplasa in sus. Deplasarea se face prin eliberarea pozitiei curente a demonului, adica prin apelarea metodei Move() din clasa Location (mai multe detalii acolo),
	 * si incercarea de a ocupa pozitia noua prin apelarea metodei Occupy tot a clasei Location. In caz ca nici asta nu merge, se retuneaza false.
	 * @return false daca demonul nu se poate deplasa in sus, true in caz contrar
	 */
	private boolean MoveUp()
	{
		if(positionX==0)
		{	
			HitWall();			
			return false;
		}
			
		if(coven.cavenLocations[positionX-1][positionY].isBusy)
			{
				return false;
			}
			
		coven.cavenLocations[positionX][positionY].Move();
		
		if(coven.cavenLocations[positionX-1][positionY].Occupy(this))
		{
			positionX--;
			
			return true;
		}
					
		return false;
		
	}
		
	/**
	 * Metoda implementeaza deplasarea demonului in jos, adica este incrementata variabila care indica pe ce linie se afla demonul. In cazul in care este un perete, se apeleaza metoda {@link HitWall()}
	 * adica se considera ca demonul a lovit un perete si se returneaza false. Daca aceea pozitie este ocupata de alt demon, de asemenea se returneaza false. Daca insa nici una din variantele prezentate nu este valida, atunci
	 * demonul se poate deplasa in jos. Deplasarea se face prin eliberarea pozitiei curente a demonului, adica prin apelarea metodei Move() din clasa Location (mai multe detalii acolo),
	 * si incercarea de a ocupa pozitia noua prin apelarea metodei Occupy tot a clasei Location. In caz ca nici asta nu merge, se retuneaza false.
	 * @return false daca demonul nu se poate deplasa in jos, true in caz contrar
	 */
	private boolean MoveDown()
	{
		if(positionX==coven.GetMatrixSize()-1)
		{	
			HitWall();			
			return false;
		}
		
		if(coven.cavenLocations[positionX+1][positionY].isBusy)
			{
				return false;
			}
		
		coven.cavenLocations[positionX][positionY].Move();
		
		if(coven.cavenLocations[positionX+1][positionY].Occupy(this))
		{			
			positionX++;
			
			return true;
		}				
			
		return false;
		
	}
	
}
