package Task2;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;

/**
 * Clasa implementeaza bariera la care toti demonii ar trebui sa se opreasca. Bariera reprezinta defapt diagonala principala. Metodele implementate sunt:
 * <p>{@link #Barrier(Coven)}</p>
 * <p>{@link #StopBarrier(Demon)}</p>
 * <p>Membrii de tip data sunt:</p>
 * <p> partiesAwait - numarul de demoni aflati in asteptare in bariera</p>
 * <p> coven - cuibul in care se afla bariera</p>
 * <p> demonsArrive - demonii aflati in asteptare si semaforul asociat lor</p> 
 * @author Marinescu Ana
 *
 */
public class Barrier {

	int partiesAwait=0;
	Coven coven;
	Map<Integer,Semaphore> demonsArrive;
	
	/**
	 * Constructorul unde se fac initializarile
	 * @param coven cuibul din care face parte bariera
	 */
	public Barrier(Coven coven) {
	
		this.coven=coven;
		
		demonsArrive=new HashMap<Integer, Semaphore>();
		
	}	
	
	/**
	 * Odata ce un demon acceseaza bariera, este adaugat in HashMap primind un semafor initializat cu 1. Se incrementeaza variabila care numara cati demoni sunt in bariera,
	 * si cat timp demonii nu au intrat toti in bariera asteapta. Odata ajunsi toti acestia sunt eliberati iar lista se goleste.
	 * @param demon demonul care acceseaza bariera
	 */
	public void StopBarrier(Demon demon) {
			
			System.out.println("The demon "+demon.GetId()+" is arrived to barrier.");
			
			demonsArrive.put(demon.GetId(),new Semaphore(1));
			
			partiesAwait++;
			
			try {
				
				if(partiesAwait<coven.GetSizeDemonsList())
				 {
					
					for(Map.Entry<Integer,Semaphore> object:demonsArrive.entrySet())
					{
						
						object.getValue().acquire();
					
					}
				 
				 }else {
					
					 partiesAwait=0;
					 
					for(Map.Entry<Integer,Semaphore> object:demonsArrive.entrySet())
					{
					
						object.getValue().release();
					
						System.out.println("The demon "+demon.GetId()+" left the barrier.");
						
					}
					
					demonsArrive.clear();
					 			
				}
			
			}catch (Exception e) {
			
				e.printStackTrace();
			
			}
			
		}
	
}
