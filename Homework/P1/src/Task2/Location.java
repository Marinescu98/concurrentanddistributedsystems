package Task2;

/**
 * Clasa reprezinta una din locatiile matricei cuibului. Este implementata ca un monitor ce sincronizeaza fiecare locatie din cuib astfel incat doi demoni sa nu se gaseasca pe
 * aceeasi pozitie intr-un cuib. Metodele necesare sunt:
 * <p>{@link #Location()} constructorul clasei</p>
 * <p>{@link #Occupy(Demon)}</p>
 * <p>{@link #Move()}</p>
 * <p>Membrii de tip data: </p>
 * <p>isBusy - boolean care determina daca un demon se afla la locatia curenta </p>
 * <p>demon - instanta a demonului care se afla la locatia curenta </p>
 * @author Marinescu Ana
 *
 */
public class Location {

	public Boolean isBusy;
	public Demon demon;
	
	/**
	 * Constructorul clasei care initializeaza variabilele astfel incat sa se stie ca la inceput pozitia e libera.
	 */
	public Location() {
		isBusy=false;
		
		demon=null;
	}
	
	/**
	 * Atunci cand un demon incearca sa ocupe o pozitie se apeleaza metoda curenta. In cazul in care un demon se alfa deja la pozitia aceasta, se considera ca cei doi s-au intalnit
	 * ceea ce inseamna ca amandoi isi cresc abilitatile sociale. Dupa care, cat timp pozitia e ocupata, demonul care vrea s-o ocupe asteapta pana cand se elibereaza. 
	 * @param demonNew instanta demonului care incearca sa ocupe locatia
	 * @return isBusy (true daca e ocupata, false daca nu)
	 */
	public synchronized Boolean Occupy(Demon demonNew)
	{
		
		if(isBusy==true)
		{
			demonNew.MeetDemon();
			demon.MeetDemon();
		}
		
		while(isBusy)
		{
			try {
				wait();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
		
		isBusy=true;
		
		demon=demonNew;
		
		return isBusy;
	}
	
	/**
	 * Metoda este apelata atunci cand un demon elibereaza o locatie. Pentru a o elibera acesta trebuie sa se deplaseze, ceea ce inseamna ca va crea un ingredient deci se 
	 * apeleaza CreateIngredient() dupa care se elibereaza si se notifica toti demonii ca au fost create ingrediente.
	 */
	public synchronized void Move()
	{
		demon.CreateIngredient();
		
		demon=null;
		
		isBusy=false;
		
		notifyAll();
	}
}
