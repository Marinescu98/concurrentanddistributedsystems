package OriginalVersion;

/**
 * Clasa War comunica cu portalul magic folosit pentru a comunica cu marele vrajitor. Atunci cand marele vrajitor are toate portiunile,
 * se comunica prin portal acest lucru, ceea ce va determina oprirea normala a threadurilor. Un thread apeleaza functia
 * WarIsEnd() ce returneaza true daca nu mai au pentru ce sa lupte caci au castigat, false daca vrajitorul nu are suficiente portiuni pentru a invinge.
 * @author Marinescu Ana
 *
 */
public class War {
	private static MagicPortal magicPortal;
	
	public War(MagicPortal magicPortal) {
		this.magicPortal=magicPortal;
	}
	
	public boolean WarIsEnd()
	{
		return magicPortal.SaveWorld();
	}
}
