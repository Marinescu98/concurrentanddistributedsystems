package OriginalVersion;

import java.util.Random;

/**
 * <p>
 * Clasa implementeaza cercul marelui vrajitor care face urmatoarele: </p>
 * <p>a)creaza cuiburile: NCOVEN reprezinta numarul de cuiburi din cerc, iar covens este un vector care salveaza toate cele NCOVEN cuiburi. NCOVEN
 * este un numar aleator din intervalul 3-20. Dupa initializare, threadurile sunt pornite.</p>
 * <p>b)creaza vrajitoarele. Se stie de la inceput ca exista 10 vrajitoare, care sunt create apaland functia CreateWitches(). In functie, fiecare
 *  vrajitoare este creata individual (nu am avut alta idee). Am considerat ca fiecare vrajitoare va avea crea doua portiuni al 
 * caror id este stiut, si doua liste de ingrediente, una pentru prima portiune si a doua pentru a doua portiune. Dupa initializare, threadurile sunt pornite</p>
 * <p>c)creaza strigoii. NUNDEAD reprezinta numarul de strigoi care este un numar aleator din intervalul 20-50. Odata ce threadurile sunt create si initializate
 * urmeaza a fi pornite</p>
 * <p>d)creaza portalul magic prin care vrajitoarele trimit portiuni vrajitorului si primesc portiuni cand sunt atacate de strigoi. Acesta nu e un thread.</p>
 * <p>e)creaza ajutorul marelui vrajitor. Acesta adauga demoni in cuiburi, e un thread si odata creat si initializat, este pornit.</p>
 * <p>f)creaza un obiect al clasei War. Tot programul va rula cat timp vrajitorul nu a primit toate portiunile de la vrajitoare. War comunica cu portalul
 * magic prin care vrajitoarele trimit portiuni pentru a sti daca vrajitorul are sau nu toata portiunile. Daca le are, atunci programul se incheie, altfel
 * threadurile isi vor continua cursul.</p>
 * @author Marinescu Ana
 *
 */

public class GrandSorcererCircle {

	static int NCOVEN;
	static Coven[] covens;
	static Witch[] witches=new Witch[10];
	static MagicPortal magicPortal=new MagicPortal();
	static Undead undeads[];
	static int NUNDEAD;
	static War war=new War(magicPortal);
	
	public static void main(String[] args) {
		
		Random randomObject=new Random();
		
		NCOVEN=randomObject.nextInt(18)+3;
		NUNDEAD=randomObject.nextInt(31)+20;
		
		System.out.println("NCOVEN = \n"+NCOVEN);
		System.out.println("NUNDEAD = \n"+NUNDEAD);
		
		covens=new Coven[NCOVEN];
		undeads=new Undead[NUNDEAD];
		
		for(int iterator=0;iterator<NCOVEN;iterator++)
		{
			covens[iterator]=new Coven(iterator,war);
			covens[iterator].start();
		}	
		
		CreateWitches();
		
		for(int iterator=0;iterator<10;iterator++)
		{
			witches[iterator].start();
		}
		
		for(int iterator=0;iterator<NUNDEAD;iterator++)
		{
			undeads[iterator]=new Undead(NCOVEN, covens, war);
			undeads[iterator].start();
		}
		
		GrandSorcererHelper grandSorcererHelper=new GrandSorcererHelper(NCOVEN, covens,war);
		
		grandSorcererHelper.start();

	}
	
	public static void CreateWitches() {
		 
		 int[] ingredients={2,3,4,5};
		 int[] ingredients2= {1,9,6,7,8};
		 witches[0]=new Witch(0,1,ingredients,ingredients2,covens,NCOVEN,magicPortal);  
		
		 int[] ingredients1={0,3,7,8};
		 int[] ingredients12= {1,2,4,9};
		 witches[1]=new Witch(2,3,ingredients1,ingredients12,covens,NCOVEN,magicPortal);
		 
		 int[] ingredients21={2,9,0,5};
		 int[] ingredients22= {1,2,5,7,8};
		 witches[2]=new Witch(4,5,ingredients21,ingredients22,covens,NCOVEN,magicPortal); 
		 
		 int[] ingredients31={2,3,0,5};
		 int[] ingredients32= {1,2,5,7,9,8};
		 witches[3]=new Witch(6,7,ingredients31,ingredients32,covens,NCOVEN,magicPortal); 
		 
		 int[] ingredients41={2,4,7,5,9};
		 int[] ingredients42= {1,2,5,0,8};
		 witches[4]=new Witch(8,9,ingredients41,ingredients42,covens,NCOVEN,magicPortal); 
		 
		 int[] ingredients51={2,4,7,0};
		 int[] ingredients52= {1,2,5,0,3,8};
		 witches[5]=new Witch(10,11,ingredients51,ingredients52,covens,NCOVEN,magicPortal);
		 
		 int[] ingredients61={8,5,7,0};
		 int[] ingredients62= {1,4,5,0,3,8,9};
		 witches[6]=new Witch(12,13,ingredients61,ingredients62,covens,NCOVEN,magicPortal);
		 
		 int[] ingredients71={8,6,9,1};
		 int[] ingredients72= {2,9,5,0,3,8,7};
		 witches[7]=new Witch(14,15,ingredients71,ingredients72,covens,NCOVEN,magicPortal);
		 
		 int[] ingredients81={8,9,5,6,7};
		 int[] ingredients82= {1,4,2,9,3,8,9};
		 witches[8]=new Witch(16,17,ingredients81,ingredients82,covens,NCOVEN,magicPortal);
		 
		 int[] ingredients91={8,0,7,9};
		 int[] ingredients92= {1,3,8,9};
		 witches[9]=new Witch(18,19,ingredients91,ingredients92,covens,NCOVEN,magicPortal);

	}

}
