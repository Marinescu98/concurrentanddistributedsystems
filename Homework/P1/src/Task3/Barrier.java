package Task3;

import java.util.concurrent.CyclicBarrier;


/**
 * Clasa implementeaza bariera la care toti demonii ar trebui sa se opreasca. Bariera reprezinta defapt diagonala principala. Metodele implementate sunt:
 * <p>{@link #Barrier(int)}</p>
 * <p>{@link #StopBarrier(Demon)}</p>
 * <p>Membrii de tip data sunt:</p>
 * <p> cyclicBarrier - instanta a clasei CyclicBarrier folosita pentru a opri threadurile pana cand toate ajung la bariera, apoi sunt eliberate</p>
 * @author Marinescu Ana
 *
 */
public class Barrier {

	private CyclicBarrier cyclicBarrier;
	
	/**
	 * Constructor 
	 * @param nMAxDemon numarul de demoni care trebuie sa ajunga la bariera
	 */
	public Barrier(int nMAxDemon) {
		cyclicBarrier=new CyclicBarrier(nMAxDemon);
	}
	
	/**
	 * Odata ce un demon acceseaza bariera,asteapta pana cand si restu ajung, dupa care sunt eliberati.
	 * @param demon demonul care acceseaza bariera
	 */
	public void StopBarrier(Demon demon) {
		
		System.out.println("The demon"+demon.GetId()+" is arrived to barrier.");
		
		try {
			
			cyclicBarrier.await();
		
		}catch (Exception e) {
		
			e.printStackTrace();
		
		}
		
		System.out.println("The demon"+demon.GetId()+" left the barrier.");
		
	}
}
