package MatrixMultiplication;

import java.util.concurrent.Future;

/**
 * Task-ul de inmultire MatrixMultiplication primeste matricile a si b, respectiv rezultatul c. Acesta clasa este un thread ce mosteneste clasa Thread si are 
 * implementate urmatoarele metode:
 * <p>{@link #MatrixMultiplication(Matrix, Matrix, Matrix, MatrixTask)}</p>
 * <p>{@link #run()}</p>
 * @author Marinescu Ana
 *
 */
public class MatrixMultiplication extends Thread {

	private Matrix a,b, c;
	private MatrixTask matrixTask;
	
	/**
	 * Constructorul clasei
	 * @param a operand
	 * @param b operand
	 * @param c rezultatul inmultirii
	 * @param matrixTask obiect de tipul MatrixTask
	 */
	public MatrixMultiplication (Matrix a, Matrix b, Matrix c, MatrixTask matrixTask)
	{
		this.matrixTask=matrixTask;
		this.a=a;
		this.b=b;
		this.c=c;
	}
	
	/**
	 *  n dimensiunea operanzilor. Inmultirea se realizeaza concurent. Pentru n=1 inseamna ca matricea a este scalar, astfel se realizeaza inmultirea dintre elementul ramas in a si
	 *  linia corespunzatoare din b.Pentru realizarea calculelor se foloseste o rezerva de fire. Matricea este creata
	 *   separat, apoi este initializata in urma operatiei submit() de trimitere a sarcinilor spre executie. Asteptarea terminarii calculelor se realizeaza invocand metoda get() a clasei Future. 
	 */
	@Override
	public void run() {
		try {
			
			int n=a.GetSize();
			
			if(n==1)
			{	
				for(int i=0;i<b.GetSize();i++)
				{
					c.add(a.rowDisplace,i,a.get(0,0)*b.get(a.colDisplace,i));
				}
				
			}else {
				
				System.out.print(".");
				
				Matrix[][] aa=a.split();
				
				Future<?>[][] future=(Future<?>[][])new Future[2][2];
				
				for(int i=0;i<2;i++)
				{
					for(int j=0;j<2;j++)
					{
						future[i][j]=matrixTask.exec.submit(new MatrixMultiplication(aa[i][j], b, c, matrixTask));
					}
				}
				
				for(int i=0;i<2;i++)
				{
					for(int j=0;j<2;j++)
					{
						future[i][j].get();
					}

				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
