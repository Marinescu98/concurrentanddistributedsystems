package MatrixMultiplication;

import java.util.Random;

/**
 * O matrice se reprezinta prin clasa Matrix care contine:
 * <p>size - dimensiunea</p>
 * <p>rowDisplace, colDisplace - deplasamentul pe linii si pe coloane</p>
 * <p>data[][] - un tablou bidimensional cu elementele matricii</p>
 * <p>Metodele implementate sunt:</p>
 * <p>{@link #Matrix(int)}</p>
 * <p>{@link #Matrix(int[][], int, int, int)}</p>
 * <p>{@link #GenerateNumberRandom()}</p>
 * <p>{@link #InitializedMatrix()}</p>
 * <p>{@link #add(int, int, int)}</p>
 * <p>{@link #get(int, int)}</p>
 * <p>{@link #set(int, int, int)}</p>
 * <p>{@link #GetSize()}</p>
 * <p>{@link #split()}</p>
 * <p>{@link #print()}</p>
 * @author Marinescu Ana
 *
 */
public class Matrix {  
	
	private int size;   
	private int[][] data; 
	public int rowDisplace, colDisplace;  
	
	/**
	 * Constructor folosit pentru crearea unei matrici de dimensiune nxn
	 * @param MATRIX_SIZE dimensiunea matricii
	 */
	public Matrix(int MATRIX_SIZE) { 
		size=MATRIX_SIZE;
		rowDisplace = colDisplace = 0;    
		data = new int[MATRIX_SIZE][MATRIX_SIZE]; 
	}    
	
	/**
	 * Constructor pentru initializarea matricii pe baza unui tablou bidimensional
	 * @param matrix tabloul bidimensional
	 * @param x deplasamentul pe linii 
	 * @param y deplasamentul pe coloane
	 * @param dim dimensiunea matricii
	 */
	public Matrix(int[][] matrix, int x, int y, int dim) {  
		data = matrix;    
		rowDisplace = x; 
		colDisplace = y;    
		size=dim;
	}   
	
	/**
	 * Metoda folosita pentru a genera random elementele matricii
	 */
	public void GenerateNumberRandom()
	{
		Random random=new Random();
		
		for(int i=0;i<size;i++)
		{
			for(int j=0;j<size;j++)
			{
				data[i][j]= random.nextInt(1500);
			}
		}
	}
	
	/**
	 * Metoda folosita pentru a initializa cu 0 elementele matricii
	 */
	public void InitializedMatrix()
	{
		for(int i=0;i<size;i++)
		{
		
			for(int j=i;j<size;j++)
			{
				data[i][j]=data[j][i]=0;
			}
		}
	}
	
	/**
	 * Metoda folsita pentru a aduna la elementul aflat pe linia row si coloana col, valoarea val 
	 * @param row linia elementului
	 * @param col coloana elementului
	 * @param value valoarea de adunat
	 */
	public void add(int row, int col, int value)
	{
		data[row+rowDisplace][col+colDisplace] += value; 
	}
	
	/**
	 * Getter pentru a obtine valoarea elementului de la coloana col si lina row 
	 * @param row  linia la care se afla
	 * @param col coloana la care se afla 
	 * @return valoarea elementului de la coloana col si lina row 
	 */
	public int get(int row, int col) {  
	
		return data[row+rowDisplace][col+colDisplace];   
		
	}   
	
	/**
	 * Setter pentru a atribui unui element al matricei o valoare value
	 * @param row linia la care se afla
	 * @param col coloana la care se afla
	 * @param value valoarea de initializat
	 */
	public void set(int row, int col, int value) {  
	
		data[row+rowDisplace][col+colDisplace] = value; 
		
	}
	 
	/**
	 * Getter
	 * @return returneaza dimensiunea matricii
	 */
	public int GetSize() { 
		 return size;
	} 
	 
	/**
	 * Metoda imparte matricea in 4 submatrici de dimensiuni egale
	 * @return tablou bidimensional de dimensiune 2x2
	 */
	public  Matrix[][] split() {  

		int newDim = size / 2;  
		
		Matrix[][] result = new Matrix[2][2];  
		
		result[0][0] = new Matrix(data, rowDisplace, colDisplace,newDim);      
		
		result[0][1] = new Matrix(data, rowDisplace, colDisplace + newDim, newDim);    
		
		result[1][0] = new Matrix(data, rowDisplace + newDim, colDisplace, newDim);      
		
		result[1][1] = new Matrix(data, rowDisplace + newDim, colDisplace + newDim, newDim);     
		
		return result;   
		
	} 
	
	/**
	 * Afiseaza matricea curenta
	 */
	public void print()
	{
		System.out.println();
		
		for(int i=0;i<size;i++)
		{	
			for(int j=0;j<size;j++)
			{
				System.out.print(data[i][j]+" ");
			}
			System.out.println();
		}
	}
 } 

