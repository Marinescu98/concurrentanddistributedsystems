package MatrixMultiplication;

/**
 * <p>
 * Clasa are implementata doar metoda main(String[]) in care initial sunt create matricile, sunt generate aleator valorile lor, afisate,
 * dupa care se face inmultirea. Membrul static de tip data  MATRIX_SIZE  reprezinta dimensiunea matricii.
 * @author Marinescu Ana
 *
 */

public class Main {

	static final int MATRIX_SIZE=4;
	
	public static void main(String[] args) {	
		Matrix matrixa=new Matrix(MATRIX_SIZE);
		Matrix matrixb=new Matrix(MATRIX_SIZE);
		
		matrixa.GenerateNumberRandom();
		matrixb.GenerateNumberRandom();
		
		matrixa.print();
		matrixb.print();
		
		MatrixTask matrixTask=new MatrixTask();
		
		try {
			
			System.out.print("\nMatrix multiplication start...");
			
			long start = System.currentTimeMillis();
			
			matrixTask.multiply(matrixa, matrixb).print();
			
			long finish = System.currentTimeMillis();
			
			System.out.println("\nfinished.");
			
			System.out.format("\nTime elapsed: %d ms\n", finish-start);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
