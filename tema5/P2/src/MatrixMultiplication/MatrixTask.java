package MatrixMultiplication;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Clasa implementeaza metoda {@link #multiply(Matrix, Matrix)}. Membrii de tip data sunt POOL_SIZE, numarul rezervelor de fire care este dat de
 * <strong>Runtime.getRuntime().availableProcessors()</strong> si exec care este un executor.
 * @author Marinescu Ana
 *
 */
public class MatrixTask {
	
	private static final int POOL_SIZE = Runtime.getRuntime().availableProcessors();
	
	public ExecutorService exec = Executors.newFixedThreadPool(POOL_SIZE);
	
	/**
	 * Taskul de inmultire primeste matricile a si b. Apelul threadului raspunzator de inmultirea matricilor este facut de un obiect Future. 
	 * Un Future ce nu intoarce o valoare se reprezinta prin Future.
	 * @param a operand
	 * @param b operand
	 * @return returneaza rezultatul inmultirii matricilor
	 * @throws InterruptedException exceptie
	 * @throws ExecutionException exceptie
	 */
	public Matrix multiply(Matrix a, Matrix b) throws InterruptedException, ExecutionException{
		
		int n=a.GetSize();
		
		Matrix result=new Matrix(n);
		
		result.InitializedMatrix();
		
		Future<?> future =exec.submit(new MatrixMultiplication(a, b, result,this));

		try {
			
			future.get();
			
			exec.shutdown();
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
}
