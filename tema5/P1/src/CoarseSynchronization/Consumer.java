package CoarseSynchronization;


/**
 *  <strong>Consumer</strong>
 *  <p>Consumer extinde clasa Thread si suprascrie metoda run(). Constructorul primeste ca parametrii numarul de threaduri consumator, NCTHREADS,
 *  id-ul consumatorului, intervalul in care se incadreaza numerele consumate, adica [MIN,MAX], o instanta a clasei 
 *  CoarseList denumita buffer si un elementsConsumed care numara cate elemente au fost consumate. </p>
 * <p> Metodele implementate sunt:</p>
 * <p>{@link #Consumer(int, int, int, int, CoarseList)}- constructorul clasei unde se fac initializari</p>
 * <p>{@link #getCounter()} - get care returneaza numarul de elemente care au fost consumate</p>
 * <p>{@link #run()} - implementeaza functionalitatea clasei </p>
 * 
 * @author Marinescu Ana
 */

public class Consumer extends Thread {   
  
	private static CoarseList buffer;   
	private static int NCTHREADS;		
	private int id;				
	private static int MAX;			
	private static int MIN;				
	private int elementsConsumed = 0;    	// elementsConsumed de elemente consumate 
	
	/**
	 * Se initilaizeaza urmatoarele variabile:
	 * @param NCTHREADS  numarul de consumatori
	 * @param id  id-ul consumatorului
	 * @param MAX  cel mai mare numar care poate fi consumat
	 * @param MIN  cel mai mic numar care poate fi consumat
	 * @param buffer  instanta a clasei CoarseList
	 */
	
	public Consumer(int NCTHREADS,int id,int MIN,int MAX,CoarseList buffer) {     
		
		this.NCTHREADS = NCTHREADS; 
	
		this.id = id;     
		
		this.MIN = MIN; 
		
		this.MAX = MAX;     
		
		this.buffer = buffer;   
		
	}   
	
	/**
	 * Getter
	* @return numarul de elemente care au fost consumate
	*/	
	int getCounter() { 
		return elementsConsumed; 		
	}   
	
	/**
	 * <p> int i - contor folosit pentru generarea elementelor de consumat</p>
	 * 
	 * <p>Contorului i este incrementat pana pana ajunge la prima valoare, adica pana cand impartirea lui la
	 * npthreads da un numar diferit de id. Cat timp i este mai mic ca MAX, se consuma un element 
	 * apeland metoda remove implementata din clasa CoarseList in cazul in care bufferul nu e gol.</p>
	 * <p>Valoarea returnata de functie este contorizata si afisata, apoi creste contorul i cu npthreads.
	 * De asemenea, pe langa contorul i, mai este incrementat si elementsConsumed, care numara cate elemente au fost consumate.</p>
	 */
	public void run() {  
		
		int i = MIN;
		
		int was_consumed;
		
		while ((i % NCTHREADS) != id) {
		
			i++; 

		}     
		
		while (i <= MAX && buffer.GetDim()>0) {       
			
			if(buffer.GetDim()>0)
			{
				was_consumed = buffer.remove(); 
			
				if(was_consumed!=-1)
				{
					elementsConsumed++;       
				
					System.out.println("Consumer "+id+" consumed "+was_consumed);      
				}
			
			
				i += NCTHREADS;    
			}
		}   
		
	} 
	
} 
