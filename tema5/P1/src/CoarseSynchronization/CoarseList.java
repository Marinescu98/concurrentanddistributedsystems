package CoarseSynchronization;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Clasa simuleaza functionalitatea unei liste simplu inlantuite, unde head reprezinta capul listei sau nodul de inceput,
 * itemsNo reprezinta numarul de noduri din lista, iar lockProd si lockCons sunt doua lockuri care gestioneaza accesul la nodurile listei al consumatorilor si producatorilor.
 * <p>Metodele clasei sunt</p>
 * <p>{@link #GetDim()}</p>
 * <p>{@link #add(int)}</p>
 * <p>{@link #remove()}</p>
 * @author Marinescu Ana
 *
 */
public class CoarseList {
	
	private Node head=null;
	private final Lock lockProd = new ReentrantLock();	
	private final Lock lockCons = new ReentrantLock();
	private static int itemsNo=0;
	
	/**
	 * Getter
	 * @return returneaza numarul de noduri din lista
	 */
	public int GetDim()
	{
		return itemsNo;
	}
	
	/**
	 * Metoda este folosita pentru a adauga un element in lista astfel: cei care pot adauga elemente in liste sunt producatori, astfel se apeleaza metoda lock pentru a bloca accesul altor producatori la lista.
	 * Se initializeaza cheia nodului cu un id unic si este creat noul nod care trebuie adaugat. Daca lista e goala, atunci capul listei este chiar noul nod, dar daca mai exista si alte noduri in lista, se foloseste un 
	 * nod pred pentru a parcurge elementele pana la ultimul. Odata ajuns la finalul listei, ultimul nod va indica catre nodul nou adaugat, astfel lista este implementata pe principiul LIFO(last in first out), adica este o stiva.
	 * La final, se elibereaza zavorul apeland unlock.
	 * @param item valoarea nodului care urmeaza a fi adaugat in lista
	 */
	public void add(int item)
	{
		
		lockProd.lock();
		
		int key=itemsNo++;
		
		try {
			
			Node node = new Node(item,key);
			
			if(head==null)
			{
				head = node;
				
			}else {
			
				Node pred = head; 
						
				while (pred.next != null) {
				
					pred = pred.next; 
					
				}
				
				pred.next = node;
			}	
			
		} finally {
			
			lockProd.unlock();
	
		}
		
	}
	
	/**
	 * Metoda este folosita pentru a extrage un element din lista astfel: cei care pot extrage elemente din lista sunt consumatorii, deci se blockeaza accesul celorlalti consumatori prin apelul metodei lock().
	 * temp este o variabila locala, temporala initializata cu -1 pentru a arata ca niciun element nu a fost extras. Daca lista e goala se returneaza -1. Daca lista are doar un nod, se extrage iar valoarea lui din campul corespunzator informatiei utile este returnata,
	 * altfel, se parcurg toate elementele pana la ultimul, iar acela va fi extras. Astfel, lista este implementata pe principiul LIFO(last in first out), adica este o stiva. 
	 * @return -1 daca lista e goala, un numar natural care reprezinta elementul consumat
	 */
	public int remove() {
			
		lockCons.lock();		
		
		int temp=-1;
		
		try {	
				Node pred = head; 
				
				if(pred==null)
				{
					return temp;
				}
				
				if(pred.next==null)
				{
					temp=pred.item;
					
					head = null;
					
					itemsNo--;
				}
				else {
				
					while (pred.next.next!=null) {
					
						pred = pred.next; 
					
					}
					
					temp=pred.next.item;
						
					pred.next = null;
					
					itemsNo--;
					
				}
			
				return temp;
				
		} finally {
			
			lockCons.unlock();
			
		}
		
		
	}
	
}
