package CoarseSynchronization;

/**
 * Clasa implementeaza un nod al listei CoarseList. Fiecarui element ii corespunde un nod in lista: item memoreaza valoarea elementului si key este un index cu valoare unica pentru fiecare nod.
 * 
 */
public class Node {

	int item;//memoreaza valoarea elementului
	
	int key;//index cu valoare unica pentru fiecare nod
	
	Node next;
	
	public Node(int item, int key) {
		
		this.item=item;
		
		this.key=key;
		
		next=null;
	}
}
