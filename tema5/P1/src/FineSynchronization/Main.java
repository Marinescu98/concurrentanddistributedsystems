package FineSynchronization;

import java.util.Random;
import java.util.Scanner;


/**
 * <p>
 * Clasa are urmatoarele metode implementate:  {@link #main(String[])}  si generateRandomNumber(int max) folosita pentru generarea de numere aleatoare. Membrii statici de tip data sunt: 
 * </p>
 * <p>MINN, MAXN - reprezinta intervalul in care se afla numerele generate de producatori</p>
 * <p>NPTHREADS - numarul maxim de threaduri producatori</p>
 * <p>NCTHREADS - numarul maxim de threaduri consumatori care reprezinta numarul de fire ale procesorului si se atribuie folosind <strong>Runtime.getRuntime().availableProcessors()</strong></p>
 * <p>pThreads[] - vectorul de threaduri producatori</p>
 * <p>cThreads[] - vectorul de threaduri consumatori</p>
 * <p>buffer - bufferul in care sunt adaugate numerele produse de producatori si de unde consumatorii extrag numerele ce urmeaza a fi consumate</p>
 * @author Marinescu Ana
 *
 */

public class Main {

	static int MINN=1;   	
	static int MAXN=0;   	
	static int NPTHREADS;	
	static int NCTHREADS=Runtime.getRuntime().availableProcessors();
	
	static Producer pThreads[];
	static Consumer cThreads[];
		
	static volatile FineList buffer = new FineList();
	
	public static int generateRandomNumber(int max) {
		Random random = new Random();
		return random.nextInt(max);
	}

	/**
	 *<p> In functia main initial se citesc datele de la tastatura in doua moduri: fie se da o limita pentru un interval [0,n] unde n e numarul dat la intrare si 
	 * apoi se genereaza random valorile variabilelor statice NPTHREADS, MINN si MAXN, fie sunt date de la tastatura. Dupa citirea datelor
	 * de la tastatura si initializarea variabilelor se defineste lungimea celor doi vectori pThreads si cThreads.</p>
	 * <p>Urmeaza initializarea threadurilor pThreads apeland constructorul corespunzator clasei Producer si punerea lor in executie prin apelarea metodei start(). 
	 * Acelasi lucru se intampla si in cazul threadurilor cThreads. Dupa ce se parcurg toti vectorii, urmeaza suspendarea executiei lor prin apelarea metodei join.
	 * Apoi, pentru fiecare consumator se afiseaza  numarul de elemente consumate si pentru fiecare producator se afiseaza numarul de elemente produse.</p>
	 * <p>Variabilele necesare sunt:</p>
	 * <p>LIMIT - limita pentru intervalul [0,LIMIT]</p>
	 * <p>i - contor</p>
	 *  
	 *  @param args	args
	**/
	
	public static void main(String[] args) { 
		
		int i;    
		
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Type 1 if you want to generate random numbers or other number if you want to add yourself:");
		
		if(sc.nextInt()==1)
		{
			System.out.println("Type the limits: ");
			final int LIMIT=sc.nextInt();
			
			while(MAXN-MINN<30)
			 {
				MINN=generateRandomNumber(LIMIT);   			
				MAXN=generateRandomNumber(LIMIT); 
			 }
			
			NPTHREADS=generateRandomNumber(LIMIT);	
			 
			while(NPTHREADS<4)
			 {
				 NPTHREADS=generateRandomNumber(LIMIT);	
			 }
				 
			 System.out.println("[MINN, MAXN] -> ["+MINN+","+MAXN+"]");
			 System.out.println("NPTHREADS = "+NPTHREADS);
			 System.out.println("NCTHREADS = "+NCTHREADS);
		
		}else {
			
			System.out.println("NCTHREADS = "+NCTHREADS);
			
			System.out.println("Type the number of producers: ");
			 NPTHREADS =sc.nextInt();
			
			System.out.println("Type the min value of elements: ");
			 MINN=sc.nextInt();   
			
			System.out.println("Type the mac value of elements: ");
			 MAXN=sc.nextInt();  
		
		}
		
		pThreads = new Producer[NPTHREADS];   
		cThreads = new Consumer[NCTHREADS]; 
		
		System.out.println("Producer - Consumer started");
		
		long start = System.currentTimeMillis();
		
		for (i=0;i<NPTHREADS;i++) {       
				pThreads[i] = new Producer(NPTHREADS, i+1, MINN, MAXN, buffer);       
				pThreads[i].start();  
			}
			
		for (i=0;i<NCTHREADS;i++) {       
				cThreads[i] = new Consumer(NCTHREADS, i+1, MINN, MAXN, buffer);       
				cThreads[i].start();     
			}  
		
		long finish = System.currentTimeMillis();
				
		for (i=0;i<NPTHREADS;i++) {       
				try { pThreads[i].join(); 
				}
				catch(InterruptedException e) {
					
				}     
			}     
			
		for (i=0;i<NCTHREADS;i++) {      
				try { cThreads[i].join(); 
				}       
				catch(InterruptedException e) {
					
				}     
			}     
				

		for (i=0; i<NPTHREADS; i++) {       
				System.out.println("Producer "+pThreads[i].GetId()+" produced "+pThreads[i].GetCounter()+" elements.");     
			}     
				

		for (i=0; i<NCTHREADS; i++) {       
				System.out.println("Consumer "+cThreads[i].GetId()+" consumed "+cThreads[i].GetCounter() +" elements.");   
					
			}  
				
		System.out.println("Producer - Consumer finished"); 
		System.out.format("\nTime elapsed: %d ms\n", finish-start);
	}
			
} 
				
