package FineSynchronization;


/**
 * Clasa simuleaza functionalitatea unei liste simplu inlantuite, unde head reprezinta capul listei, adica o valoare mai mare decat orice valoare care poate aparea in lista, iar tail este coada, adica o valoare mai mica decat orice valoare care poate aparea in lista.
 * Se presupune ca orice lista contine aceste doua noduri suplimentare numite santinele: head si tail si ca nu reprezinta elemente adaugate, eliminate sau cautate in lista. 
 * <p>Metodele clasei sunt</p>
 * <p>{@link #add(Object)}</p>
 * <p>{@link #remove()}</p>
 * <p>{@link #IsEmplty()}</p>
 * <p>{@link #FineList()}</p>
 * @author Marinescu Ana
 *
 */
public class FineList<T> {
	
	private Node<T> head, tail;
	
	/**
	 * Getter
	 * @return returneaza true daca lista e goala sau false daca exista elemente in lista
	 */
	public boolean IsEmplty() {
		return head.next==tail;
	}
	
	/**
	 * Constructorul clasei
	 */
	public FineList()
	{
		head = new Node(Integer.MIN_VALUE);
		head.next=tail=new Node<>(Integer.MAX_VALUE);
	}
	
	/**
	 * Pentru evitarea interblocarii firele trebuie sa achizitioneze zavoarele in aceeasi ordine, de la head in spre tail. crt reprezinta nodul curent iar pred  reprezinta predecesorul nodului curent.
	 * Se presupune ca elementele sunt in ordine crescatoare in lista astfel ca se cauta pozitia potrivita pentru fiecare nod. 
	 * @param item valoarea nodului de adaugat
	 * @return true daca nodul a fost adaugat sau false in caz contrar
	 */
	public boolean add(T item)
	{
		int key=item.hashCode();
		
		head.lock();
		
		Node<T> pred=head, crt;
					
		crt=pred.next;
					
		crt.lock();
						
		while (crt.key<key) {
											
			pred = crt;
							
			crt=crt.next;
						
			crt.lock();
			
		}
							
		if(crt.key==key) {
			return false;
		}
						
		Node<T> newNode=new Node(item);
						
		newNode.next=crt;
						
		pred.next=newNode;
						
		return true;
						
	}
	
	/**
	 * Metoda extrage un nod din lista, unde crt reprezinta nodul curent iar pred  reprezinta predecesorul nodului curent.
	 * @return -1 daca nu s-a extras niciun nod, sau o valoare naturala care reprezinta valoare nodului extras
	 */
	public int remove() {
		
		Node<T> pred =head, crt=null;
		
		head.lock();	
			
		pred=head;
			
		crt=pred.next;
			
		crt.lock();
					
		if(!IsEmplty())
		{		
			int temp = crt.key;
					
			pred.next=crt.next;
					
			return temp;
		}
		else {
					
			return -1;
		}
		
	}
	
}
