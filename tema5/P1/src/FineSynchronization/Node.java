
package FineSynchronization;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Clasa implementeaza un nod al listei FineList. Fiecarui element ii corespunde un nod in lista: item memoreaza valoarea elementului si key este un index cu valoare unica pentru fiecare nod.
 * next este o instanta a nodului urmator iar isLocked este un integer care are initial valoarea -1, apoi 1 cand un zavor a fost achizitionat si 2 cand a fost eliberat. Metodele implementate sunt:
 * <p>{@link #Node(int)}</p>
 * <p>{@link #Node(Object)}</p>
 * <p>{@link #lock()}</p>
 * <p>{@link #unlock()}</p>
 */
public class Node<T> {

	private Lock lock;
	T item;//memoreaza valoarea elementului
	int key;//index cu valoare unica pentru fiecare nod
	Node<T> next;
	int isLocked=-1;
	
	/**
	 * constructor 
	 * @param item informatia utila a nodului
	 */
	public Node(T item) {
		this.item=item;
		this.key=item.hashCode();
		lock=new ReentrantLock();
		next=null;
	}
	
	/**
	 * Constructor
	 * @param key cheia unica specifica nodului
	 */
	public Node(int key) {
		this.item=null;
		this.key=key;
		lock=new ReentrantLock();
		next=null;
	}
	
	/**
	 * Metoda achizitioneaza zavorul nodului daca nu a mai fost achizitionat sau a fost eliberat
	 */
	public void lock() 
	{
		if(isLocked==2 || isLocked==-1)
		{
			lock.lock();
			isLocked=1;
		}

	}
	
	/**
	 * Metoda elibereaza zavorul nodului daca a fost achizitionat
	 */
	public void unlock() {
		
		if(isLocked==1)
		{
			lock.unlock();
			isLocked=2;
		}
		
	}
}
