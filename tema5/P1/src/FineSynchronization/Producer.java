package FineSynchronization;


/**
 *  <strong>Producer</strong>
 *  Clasa extinde clasa Thread. Constructorul primeste ca parametrii numarul de threaduri producatori,
 *  id-ul producatorului, intervalul in care se incadreaza numerele produse, adica [MIN,MAX], o instanta a clasei 
 *  PCLock si un contor care numara cate elemente au fost produse. Metodele implementate sunt:
 *  <p>@link #Producer()- constructorul clasei unde se fac initializari</p>
 *  <p>@link #getCounter() -get care returneaza cat a produs fiecare producator</p>
 *  <p>@link #run()</p>
 * <p>{@link #GetId()}</p>
 * @author Marinescu Ana
 *
 */

public class Producer extends Thread {  
	
	private static int NPTHREADS;			
	private int id;					
	private static int MAX;				
	private static int MIN;				
	private static FineList buffer;    	
	private int elementsProduced = 0;       // Contor de elemente trimise   
	
	/**
	 * Se initilaizeaza urmatoarele variabile:
	 * @param NPTHREADS  numarul de producatori
	 * @param id  id-ul producatorului
	 * @param MAX  cel mai mare numar care poate fi produs
	 * @param MIN  cel mai mic numar care poate fi produs
	 * @param buffer  instanta a clasei FineList
	 */
	public Producer(int NPTHREADS, int id, int MIN, int MAX, FineList buffer) {
		
		this.NPTHREADS = NPTHREADS; 
		
		this.id = id;     
		
		this.MIN = MIN; 
		
		this.MAX = MAX;     
		
		this.buffer = buffer;   
	}
 
	
	/**
	 * @return numarul de elemente care au fost produse
	 */
	int GetCounter() 
	{ 
		return elementsProduced; 
	}   
	
	/**
	 * Getter
	 * @return returneaza idul producatorului
	 */
	public int GetId() 
	{ 
		return id; 
	} 
	
	/**
	 * <p>int i este un contor pentru a determina valoarea elementului care va fi produs</p>
	 * 
	 * Initial este determinat primul numar produs, dupa care sunt generate urmatoarele atata timp cat valoarea lor
	 * este mai mica sau egala cu MAX. Adaugarea lor in buffer se face cu functia add, metoda apratinand clasei
	 * FineList. Odata ce un produs este adaugat in buffer, se incrementeaza  variabila care tine evidenta numarului
	 * de valori produse.
	 */
	public void run() {     

		int i = MIN;     
		
		while ((i % NPTHREADS) != id) { 
			
			i++; 
			
		} 
		
		while (i <= MAX) {       
			
			buffer.add(i); 
			
			elementsProduced++;
			
			System.out.println("Producer "+id+" produced "+i);
			
			i += NPTHREADS;
			
		}
		
	}
	
} 
	
