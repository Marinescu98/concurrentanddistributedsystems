package Philosopher;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;


/**
 *  <strong>Channel</strong>
 *  Channel are urmatoarele metodele implementate:
 *  <p>{@link #Channel()} - constructorul clasei unde se fac initializari</p>
 *  <p>{@link #send(boolean value)} - functia de send </p>
 *  <p>{@link #receive()} - functia de receive </p>
 * 
 * <p>Membrii de tip data sunt:</p>
 * <p>*queueMessages - reprezinta mesajele venite de pe canal</p>
 * <p>*monitor - variabila folosita pentru sincronizarea comunicarii</p>
 *
 * @author Marinescu Ana
 */
public class Channel {

    private BlockingQueue<Boolean> queueMessages;
    private Object monitor;

	/**
	 * Constructorul in care se definesc instantele necesare. BlockingQueue este o coada care se blocheza atunci cand se extrage din ea un element si ea este goala
	 *  sau daca se incearca sa se adauge in ea si coada este deja plina. Un thread care incearca sa aduca un element intr-o coada completa este blocat fie pana cand
	 *   un alt thread face loc elementului in coada, fie eliminand unul sau mai multe elemente, fie stergand complet coada.
	 */
    public Channel() {
        
    	queueMessages = new ArrayBlockingQueue(1);
        
    	monitor = new Object();  
    
    }

   /**
   * Prin intermediul instantei monitor se determina cand canalul este disponibil pentru comunicare, iar odata ce este disponibil, este adaugat �n coada 
   * mesajul trimis si se elibereza canalul prin apelul metodei wait care primeste ca parametru timpul maxim de asteptare in milisecunde.
   * @param value mesajul trimis pe canal
   * @return false daca mesajul a fost trimis sau false in cazul contrar
   */
    public boolean send(boolean value) { 
    	
    	Boolean dummy = new Boolean(value);
        
    	try {
           
        	synchronized (monitor) {

            queueMessages.put(dummy);
                
            monitor.wait(150);
            
        	}
       
        }catch(Exception ex) {
          
        	ex.printStackTrace();
        
        }
            
        try {
             
        	queueMessages.remove();
               
        	return false;
           
        } catch (Exception ex2) {
               
            return true;
            
        }
        
    }

    /**
     * Mesajul este extras din coada, si returnat. Canalul este trecut in starea ready. In cazul in care coada este goala, se reurneaza false.
     * @return mesajul ce trebuie transmis sau false in cazul in care coada este goala
     */
    public boolean receive() {
        try {
            
            Boolean dummy = queueMessages.take();
            
            synchronized (monitor) {
            
            	monitor.notify();
           
            }
            
            return dummy.booleanValue();
        
        } catch (InterruptedException ex) {
           
        	ex.printStackTrace();
        
        }
          
        return false;      

    }

}