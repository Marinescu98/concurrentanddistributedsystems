package Philosopher;

/**
 *  <strong>Fork</strong>
 *	<p>Fork extinde clasa Thread si suprascrie metoda run() </p>
 * 	<p> Metodele implementate sunt:</p>
 * 	<p>{@link #Fork(Channel LeftChannel, Channel RightChannel)} - constructorul clasei unde se fac initializari</p>
 *  <p>{@link #run()} - metoda suprascrisa care implementeaza functionalitatea procesului</p>
 *  <p>{@link #stopThread()} - metoda apelata pentru a opri schimbul de mesaje dintre furcultia si filosof </p>
 *  
 *  <p>Membrii de tip data sunt:</p>
 *  <p>*id - reprezinta id-ul filosofului</p>
 *  <p>*isTake - variabila booleana care determina cand o furculita este luata(true) sau nu(false)</p>
 *  <p>*dummy - variabila booleana care retine mesajul primit pe canal</p>
 *  <p>*leftChannel, rightChannel - canalele prin care comunica cu filosoful</p>
 * 
 * 	@author Marinescu Ana
 */

public class Fork extends Thread {

    private boolean isTake;
    private boolean dummy;
    private Channel leftChannel, rightChannel;

    /**
     * Constructorul in care se fac initializari.
     * @param LeftChannel canalul prin care comunica cu filosoful
     * @param RightChannel canalul prin care comunica cu filosoful
     */
    public Fork( Channel LeftChannel, Channel RightChannel) {
        this.leftChannel = LeftChannel;
        this.rightChannel = RightChannel;
        this.isTake = true;
    }

    /**
     * Metoda apelata pentru a opri schimbul de mesaje dintre furcultia si filosof.
     */
    public void stopThread() {
        isTake = false;
    }

    /**
     * Metoda run implementeza functionalitate procesului: atata timp cat furculita este luata, aceasta primeste mesaje de pe canale de la filosofi. 
     */
    @Override
    public void run() {
        
    	isTake = true;

        while (isTake) {  
         
            if (leftChannel.send(true)) {
             
                dummy = leftChannel.receive();
            }

            if (rightChannel.send(true)) {
              
                dummy = rightChannel.receive();
            }

       }
    }

}