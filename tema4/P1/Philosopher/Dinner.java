package Philosopher;

import java.time.Duration;
import java.time.Instant;
import java.util.Random;
import java.util.Scanner;

/**
 * <strong>DinnerMain</strong>
 * <p>Clasa implementeaza metoda  {@link #main(String[] args)}</p>
 * <p>Clasa implementeaza metoda  {@link #generateRandomNumber(int max)} pentru generarea unor numere aleatoare</p>
 *
 * @author Marinescu Ana
 *
 */

public class Dinner{

	/**
	 *<p> In functia main initial se citesc datele de la tastatura in doua moduri: fie se da o limita pentru un interval [0,n] unde n e numarul dat la intrare si 
	 * apoi se genereaza random valorile variabilelor statice NUMBER_OF_PHILOSOPHERS_AND_FORKS si FOOD_LEFT, fie sunt date de la tastatura pentru toate. Dupa citirea datelor
	 * de la tastatura si initializarea variabilelor se definesc cei trei vectori philosophers,channels si forks, si se pornesc threadurile</p>
	 * <p>Variabilele necesare sunt:</p>
	 * <p>NUMBER_OF_PHILOSOPHERS_AND_FORKS - numarul de filosofi si furculite </p>
	 * <p>FOOD_LEFT - cantitatea de mancare disponibila pentru toti filosofii</p>
	 * <p>philosophers - vectorul de threaduri Philosopher</p>
	 * <p>forks - vectorul de threaduri Fork</p>
	 * <p>channels - array de instante ale clasei Channel</p>
	 * <p>i - contor</p>
	 *  
	 *  @param args	args
	**/
    public static void main(String[] args) {
 
        int NUMBER_OF_PHILOSOPHERS_AND_FORKS;

        int FOOD_LEFT;
        
        Scanner scannerObject=new Scanner(System.in);
		
		System.out.println("Type 1 if you want to generate random numbers or other number if you want to add yourself:");
		
		if(scannerObject.nextInt()==1)
		{
			System.out.println("Type the limits: ");
			
			int limits=scannerObject.nextInt();
			
			NUMBER_OF_PHILOSOPHERS_AND_FORKS=generateRandomNumber(limits);
			
			FOOD_LEFT=generateRandomNumber(limits);
			
			 System.out.println("NUMBER_OF_PHILOSOPHERS_AND_FORKS = "+NUMBER_OF_PHILOSOPHERS_AND_FORKS);
			 
			 System.out.println("FOOD_LEFT = "+FOOD_LEFT);
			
		}else {
			
			System.out.println("Type the number of forks and philosophers: ");
			
			NUMBER_OF_PHILOSOPHERS_AND_FORKS=scannerObject.nextInt();
			
			System.out.println("Type the food that the philosopher can eat: ");
			
			FOOD_LEFT=scannerObject.nextInt();
		}
		
        
		System.out.println("Dinner is starting!");
		
		Instant start=Instant.now();
        
		Channel channels[] = new Channel[2 * NUMBER_OF_PHILOSOPHERS_AND_FORKS];
        
		for (int i = 0; i < 2*NUMBER_OF_PHILOSOPHERS_AND_FORKS; i++) {
            channels[i] = new Channel();
        }

        Fork forks[] = new Fork[NUMBER_OF_PHILOSOPHERS_AND_FORKS];
        
        for (int i = 0; i < NUMBER_OF_PHILOSOPHERS_AND_FORKS; i++) {
            forks[i] = new Fork(channels[2 * i], channels[2 * i + 1]);
        }

        Philosopher philosophers[] = new Philosopher[NUMBER_OF_PHILOSOPHERS_AND_FORKS];
        
        for (int i = 0; i < NUMBER_OF_PHILOSOPHERS_AND_FORKS; i++) {
            boolean forkOrder = true;
            if (i % 2 == 0) {
                forkOrder = false;
            }
            int leftFork = 2 * i - 1;
            if (leftFork < 0) {
                leftFork += NUMBER_OF_PHILOSOPHERS_AND_FORKS * 2;
            }
            int rightFork = 2 * i;
            philosophers[i] = new Philosopher(i, channels[leftFork], channels[rightFork], forkOrder,FOOD_LEFT);
             }

        for (int i = 0; i < NUMBER_OF_PHILOSOPHERS_AND_FORKS; i++) {
            forks[i].start();
        }

        for (int i = 0; i < NUMBER_OF_PHILOSOPHERS_AND_FORKS; i++) {
            philosophers[i].start();
        }
        
        Instant finish=Instant.now();
        for (int i = 0; i < NUMBER_OF_PHILOSOPHERS_AND_FORKS; i++) {
            try {
                philosophers[i].join();
            }catch (InterruptedException ex) {
                
            }
        }
        
        for (int i = 0; i < NUMBER_OF_PHILOSOPHERS_AND_FORKS; i++) {
            forks[i].stopThread();
        }
        

        System.out.println("Dinner is over!");	
        
        System.out.println("\n Duration: "+Duration.between(start, finish).toMillis()+" milliseconds");

    }

    public static int generateRandomNumber(int max) {
		
		Random random = new Random();
		
		return random.nextInt(max);
	}
}