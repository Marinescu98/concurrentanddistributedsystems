package Philosopher;

/**
 *  <strong>Philosopher</strong>
 *  Philosopher extinde clasa Thread si suprascrie metoda run(). Metodele implementate sunt:
 *  <p>{@link #Philosopher(int idPhilosopher, Channel channelLeft, Channel channelRight, boolean forkOrder,int foodleft)} - constructorul clasei unde se fac initializari</p>
 *  <p>{@link #think()} </p>
 *  <p>{@link #eat()} </p>
 *  <p>{@link #run()} </p>
 *  <p>{@link #pickUpLeftFork()}</p>
 *  <p>{@link #pickUpRightFork()}</p>
 *  <p>{@link #putDownFork()}</p>
 * 
 * <p>Membrii de tip data sunt:</p>
 * <p>*channelLeft - reprezinta canalul prin care filosoful comunica cu furculia stanga</p>
 * <p>*channelRight - reprezinta canalul prin care filosoful comunica cu furculia dreapta</p>
 * <p>*idPhilosopher - reprezinta id-ul filosofului curent</p>
 * <p>*forkOrder - reprezinta ordinea in care sunt luate furculitele: daca e true, atunci furculita dreapta e ridicata prima, altfel cea stanga</p>
 * <p>*dummy - mesajul pe care il primeste de pe canal, care este de tip boolean</p>
 * <p>*foodLeft - cantitatea de mancare comuna tuturor filosofilor</p>
 * 
 * @author Marinescu Ana
 */

public class Philosopher extends Thread {

	private Channel channelLeft;
    private Channel channelRight;
    private int idPhilosopher;
    private boolean forkOrder;
    private boolean dummy; 
    private volatile static int foodLeft;

    /**
     * In constructor se fac initializarile corespunzatoare
     * @param idPhilosopher reprezinta id-ul filosofului curent
     * @param channelLeft reprezinta canalul prin care filosoful comunica cu furcultia stanga
     * @param channelRight reprezinta canalul prin care filosoful comunica cu furcultia dreapta
     * @param forkOrder reprezinta ordinea in care sunt luate furculitele
     * @param foodleft cantitatea de mancare comuna tuturor filosofilor
     */
    public Philosopher(int idPhilosopher, Channel channelLeft, Channel channelRight, boolean forkOrder,int foodleft) {
        this.idPhilosopher = idPhilosopher;
        this.channelLeft = channelLeft;
        this.channelRight = channelRight;
        this.forkOrder = forkOrder;
        this.foodLeft=foodleft;
    }

    /**
     * Atata timp cat mancarea nu s-a terminat, fiecare filosof gandeste. Cand i se face foame, verifica care furculita e disponibila astfel:
     * daca forkOrder e true, atunci furculita dreapta e ridicata prima, altfel cea stanga. Pentru ridicare furculitelor se apeleaza functiile {@link #pickUpLeftFork()} si
     * {@link #pickUpRightFork()}. Odata ce termina de mancat, furculitele se lasa jos, ceea ce inseamna ca este apelata functia {@link #putDownFork()}. La final
     * se afiseaza un mesaj corespunzator pentru a cand un filosof terminat de mancat. 
     */
    @Override
    public void run() {
    	
        while (foodLeft>0) {
        	
            think();
            
            if (forkOrder) {
            	
                pickUpLeftFork();
                
                pickUpRightFork();

            } else {
            	
                pickUpRightFork();
                
                pickUpLeftFork();
                
            }
            
            eat();
            
            putDownFork();
            
        }
            System.out.println("Philosopher "+idPhilosopher + " is done eating.");
        
    }

    /**
     * Metoda implementeaza momentul cand un filosof mananca prin apelarea metodei sleep din clasa Thread, afisarea unui mesaj corespunzator si decrementarea
     * cantitatii de mancare disponibile pentru toti filosofii.
     */
    public void eat() {
        try {
        	
        	Thread.sleep((int)(Math.random()*100));
        	
        	System.out.println("Philosopher "+idPhilosopher+" eats");
	    	
	    	foodLeft--;
            
        } catch (InterruptedException ex) {
           ex.printStackTrace();
        }
    }

    /**
     * Metoda implementeaza momentul cand un filosof gandeste prin apelarea metodei sleep din clasa Thread si afisarea unui mesaj corespunzator.
     */
    public void think() {

        try {
        	
        	System.out.println("Philosopher "+idPhilosopher+" is thinking");
        	
        	Thread.sleep((int)(Math.random()*100));
        	
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Se primeste un mesaj de la canalul corespunzator furculitei din stanga, si este afisat un mesaj corespunzator.
     */
    public synchronized void pickUpLeftFork() {
     
        dummy = channelLeft.receive();
        
        System.out.println("Philosopher "+idPhilosopher+" picked up left fork");
    }

    /**
     * Se primeste un mesaj de la canalul corespunzator furculitei din dreapta, si este afisat un mesaj corespunzator.
     */
    public synchronized void pickUpRightFork() {

        dummy = channelRight.receive();
        
        System.out.println("Philosopher "+idPhilosopher+" picked up right fork");
    }

    /**
     * Se trimite un mesaj catre canalul corespunzator furculitei din stanga si stanga prin apelarea metodei send(), si este afisat un mesaj corespunzator pentru
     * ambele situatii.
     */
    public synchronized void putDownFork() {
        
        channelLeft.send(true);
        
        System.out.println("Philosopher "+idPhilosopher+" puts down left fork");
        
        channelRight.send(true);
        
        System.out.println("Philosopher "+idPhilosopher+" puts down right fork");
    }


   
}