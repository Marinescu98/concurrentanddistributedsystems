package ReaderWriter;

import java.util.concurrent.Semaphore;

/**
 *   <strong>Writer</strong>
 *  Writer extinde clasa Thread si suprascrie metoda run(). Constructorul primeste ca parametrii 
 *  id-ul writerului si obiectul Semaphore corespunzator lui,s_writer.
 *  Metodele implementate sunt:
 *  <p>{@link #Writer(int id_writer, Semaphore s_writer)} - constructorul clasei unde se fac initializari</p>
 *  <p>{@link #Write()} - implementeaza operatia de scriere</p>
 *  <p>{@link #run()} - implementeaza functionalitatea clasei </p>
 * 
 * @author Marinescu Ana
 *
 */
public class Writer extends Thread{

	private int id_writer;
	private Semaphore s_writer;
	
	/**
	 * Constructorul clasei in care se vor face initializarile variabilelor.
	 * @param id_writer id-ul scriitorului
	 * @param s_writer  obiect de tip Semaphore folosit pentru gestionarea permisiitor pentru scriitori
	 */
	public Writer(int id_writer, Semaphore s_writer) {
		
		this.id_writer=id_writer;
		
		this.s_writer=s_writer;
	}
	
	/**
	 * Metoda Write() implementeaza operatia de scriere a resursei.
	 */
	public void Write()
	{			
		try {
				System.out.println("The resource is written by Writer "+id_writer);
			
				Thread.sleep((int)(Math.random()*1000));
						
				System.out.println("The Writer "+id_writer+ " has finished writing\n");
		
		}catch (Exception e) {
			e.printStackTrace();	
		}
	}
	
	/**
	 * Metoda defineste procesul de scriere. Daca un scriitor vrea sa aceseze
	 * resursa, se ofera permisii acestuia, si se blocheaza accesul pentru restul 
	 * cititorilor sau scriitorilor pana cand operatia de scriere este incheiata.
	 */
	public void run() {
			try {
            	
				s_writer.acquire();
				
				Write();
				
				s_writer.release();

					            
			} catch (InterruptedException ex) { 
				    ex.printStackTrace();	            
			}	
	}
}
