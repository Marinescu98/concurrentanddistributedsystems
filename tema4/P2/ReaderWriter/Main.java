package ReaderWriter;

import java.time.Duration;
import java.time.Instant;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.Semaphore;

/**
 * <strong>Main</strong>
 * <p>
 * Clasa implementeaza metoda  {@link #main(String[] args)} de unde se citesc inputurile si se creeaza si pornesc threadurile,
 * metoda {@link #generateRandomNumber(int max)} pentru generarea de input random si {@link #join()} pentru a suspenda
 * executia threadurilor care inca mai ruleaza. 
 * </p>
 *
 * @author Marinescu Ana
 *
 */
public class Main {

	
	static int NRTHREADS;	
	static int NWTHREADS;
	
	static Reader rThreads[];
	static Writer wThreads[];

	private static Semaphore s_writer=new Semaphore(1); 	
	private static Semaphore s_reader=new Semaphore(1);
	
		/**
	 *<p> In functia main initial se citesc datele de la tastatura in doua moduri: fie se da o limita pentru un interval [0,n] unde n e numarul dat la intrare si 
	 * apoi se genereaza random valorile variabilelor statice NRTHREADS si NWTHREADS, fie sunt date de la tastatura. Dupa citirea datelor
	 * de la tastatura si initializarea variabilelor se defineste lungimea celor doi vectori rThreads si wThreads.</p>
	 * <p>Urmeaza initializarea threadurilor wThreads apeland constructorul corespunzator clasei Writer si punerea lor in executie prin apelarea metodei start(). 
	 * Acelasi lucru se intampla si in cazul threadurilor rThreads. Dupa ce se parcurg toti vectorii, urmeaza suspendarea executiei lor prin apelarea metodei join().
	 * Dupa suspendarea threadurilor se afiseaza durata procesului de creare si pornire a threadurilor in milisecunde, folosind obiectele de tip Instance si Duration din
	 * pachetul java.time.</p>
	 * <p>Variabilele necesare sunt:</p>
	 * <p>NWTHREADS - numarul de threaduri scriitori </p>
	 * <p>NRTHREADS - numarul de threaduri cititiori</p>
	 * <p>wThreads - vectorul de threaduri scriitori</p>
	 * <p>rThreads - vectorul de threaduri cititori</p>
	 * <p>s_writer - obiect de tip Semaphore corespunzatori scriitorilor</p>
	 * <p>s_reader - obiect de tip Semaphore corespunzatori cititorilor</p>
	 * <p>LIMIT - limita pentru intervalul [0,LIMIT]</p>
	 * <p>i - contor</p>
	 * <p> start, finish - instante ale clasei Instance folosite pentru a determina timpul de executie al procesului</p>
	 *  
	 *  @param args	args
	**/
	public static void main(String[] args) {
		
		int i;    
		
		Scanner sc=new Scanner(System.in);
		
		System.out.print("Type 1 if you want to generate random numbers or other number if you want to add yourself: ");
		
		if(sc.nextInt()==1)
		{
			System.out.print("Type the limits: ");
			final int LIMIT=sc.nextInt();
					 
			NRTHREADS=generateRandomNumber(LIMIT);			
			NWTHREADS=generateRandomNumber(LIMIT);
			 
			 System.out.println("NRTHREADS = "+NRTHREADS);
			 System.out.println("NWTHREADS = "+NWTHREADS);
		
		}else {
			System.out.print("Type the number of writers: ");
			 NWTHREADS=sc.nextInt();
			
			System.out.print("Type the number of readers: ");
			 NRTHREADS =sc.nextInt();
			 		
		}
		
		wThreads = new Writer[NWTHREADS];   
		rThreads = new Reader[NRTHREADS]; 
		
		System.out.println("Writer - Reader started");
		
		Instant start=Instant.now();
		
		for (i=0;i<NWTHREADS;i++) {       
			wThreads[i] = new Writer(i, s_writer);       
			wThreads[i].start();  
		}
			
		for (i=0;i<NRTHREADS;i++) {       
			rThreads[i] = new Reader(i, s_writer,s_reader);       
			rThreads[i].start();  
		}  
		
		Instant finish=Instant.now();
		
		join();
			
		System.out.println("\nWriter - Reader finished"); 
		
		System.out.println("\n Duration: "+Duration.between(start, finish).toMillis()+" milliseconds");
	}
	
	/**
	 * Metoda este folosita pentru a determina un numar ales aleator din multimea numerelor naturale cuprinse intre
	 * [0,max]
	 * @param max limita superioara a intervalului [0,max] din care va fi aleasa o valoare aleator
	 * @return o valoare aleatoare
	 */
	public static int generateRandomNumber(int max) {
		Random random = new Random();
		return random.nextInt(max);
	}

	/**
	 * Metoda este folosita pentru a suspenda threadurile, prin apelul metodei join() din clasa Threads
	 */
	public static void join()
	{
		int i;
		
		for (i=0;i<NWTHREADS;i++) {       
			try { 
				wThreads[i].join(); 
			}
			catch(InterruptedException e) {}     
		}     
				
		for (i=0;i<NRTHREADS;i++) {      
			try { 
				rThreads[i].join(); 
			}       
			catch(InterruptedException e) {}     
		}   
	}

}
