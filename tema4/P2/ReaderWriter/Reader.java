package ReaderWriter;

import java.util.concurrent.Semaphore;

/**
 *   <strong>Reader</strong>
 *  Reader extinde clasa Thread si suprascrie metoda run(). Constructorul primeste ca parametrii 
 *  id-ul readerului, obiectul Semaphore corespunzator lui, s_reader, si cel al writerului,s_writer.
 *  Varibaila statica is_read_by este folosita pentru a putea sti daca vreun cititor acceseaza resursa. 
 *   Metodele implementate sunt:
 *  <p>{@link #Reader(int id_reader, Semaphore s_writer, Semaphore s_reader)} - constructorul clasei unde se fac initializari</p>
 *  <p>{@link #Read()} -  implementeaza operatia de citire</p>
 *  <p>{@link #run()} - implementeaza functionalitatea clasei </p>
 * 
 * @author Marinescu Ana
 *
 */
public class Reader extends Thread{
	
	private Semaphore s_writer; 	
	private Semaphore s_reader;
	private int id_reader;
	private static int is_read_by=0;
	
	/**
	 * Constructorul clasei in care se vor face initializarile variabilelor.
	 * @param id_reader id-ul readerului
	 * @param s_writer obiect de tip Semaphore folosit pentru gestionarea permisiitor pentru scriitori
	 * @param s_reader obiect de tip Semaphore folosit pentru gestionarea permisiitor pentru cititori
	 */
	public Reader(int id_reader, Semaphore s_writer, Semaphore s_reader) {
		
		this.id_reader=id_reader;
		
		this.s_reader=s_reader;
		
		this.s_writer=s_writer;
	}
	
	/**
	 * Asa cum am spuns,s_reader si s_writer sunt semafoare initializate cu 1,
	 * care asigura excluderea mutuala si gesioneaza mecanismul de scriere fiind
	 * comune tuturor obiectelor de tip Reader si Writer. Variabila is_read_by
	 * indica numarul de cititori care acceseaza obiectul. Odata ce este 1, 
	 * writerul este pus in asteptare, ceea ce inseamna ca nu mai poate accesa obiectul.
	 * Dupa efectuarea operatiei de citire, variabila este decrementata, si odata ce 
	 * aceasta devine 0, se ofera acces scriitorilor.
	 */
	public void run() {
		
		try {         
				s_reader.acquire();     
					
				is_read_by++;
					
				if(is_read_by==1)
					{
						s_writer.acquire();
					}
					
				s_reader.release();
					
				Read();
					
				s_reader.acquire();
					
				is_read_by--;
					
				if(is_read_by==0)
				{
					s_writer.release();
				}
					
				s_reader.release();
				
		} catch (InterruptedException e){
					
			e.printStackTrace();
					
		}
	}
		
	/**
	 * Metoda Read() implementeaza  operatia de citire a resursei.
	 */
	public void Read()
	{
		try {
				System.out.println("The resource is read by Reader "+id_reader);
					
				Thread.sleep((int)(Math.random()*1000));
					
				System.out.println("The Reader "+id_reader+ " has finished reading");
		
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
