package Locks;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *  <strong>PCLock</strong>
 *  Metodele clasei sunt urmatoarele:
 *  <p>@link #append() </p>
 *  <p>@link #take()</p>
 * 	
 * <p>Membrii de tip data sunt urmatorii:</p>
 * <p>length_buffer - reprezinta lungimea bufferului</p>
 * <p>buffer[] - pastreaza elementele </p>
 * <p>count - numara elementele din buffer</p>
 * <p>oldest, newest - reprezinta pozitiile elementelor</p>
 * <p>lockProd - instanta a clasei ReentrantLock </p>
 * <p>lockConds - instanta a clasei ReentrantLock</p>
 * 
 * @author Marinescu Ana
 *
 */

public class PCLock {
	
	final int length_buffer = 5;	
	int oldest = 0, newest = 0;	
	volatile int count = 0;	
	int buffer[] = new int[length_buffer];
	private final Lock lockProd = new ReentrantLock();	
	private final Lock lockCons = new ReentrantLock();

	
	/**
	 * Initial se blocheaza apelantul prin apelarea functiei lock() pana cand se elibereaza zavorul prin apelarea metodei unlock().
	 * Intre timp adaugam elementul primit ca parametru in buffer si crestem numarul de elemente din buffer, count si apoi
	 * se  eliberara zavor.
	 * 
	 * @param v elementul generat ce urmeaza a fi adaugat in buffer
	 */
	void append(int v) {
	
		lockProd.lock(); 

		try {
		
			buffer[newest] = v;
			
			newest = (newest + 1) % length_buffer;
			
			count++;
		}
		finally {
		
			lockProd.unlock(); 
			
		}

	}
	
	/**
	 * Initial se blocheaza zavorul. Extragem elementul din buffer si il atribuim variabilei temporale temp si decrementam 
	 * numarul de elemente din buffer, count, apoi se apeleaza unlock() pentru eliberarea zavorului si
	 * returnam valorea extrasa din buffer.
	 * 
	 * @return valorea extrasa din buffer
	 */
	int take() {
				
		int temp;
		
		lockCons.lock();
		
		try {
		
			temp = buffer[oldest];
			
			oldest = (oldest + 1) % length_buffer;
			
			count--;
		}
		finally {
		
			lockCons.unlock();
		}
			
		return temp;
	}


}