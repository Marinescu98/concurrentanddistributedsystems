package Locks;

/**
 *  <strong>Producer</strong>
 *  Clasa extinde clasa Thread. Constructorul primeste ca parametrii numarul de threaduri producatori,
 *  id-ul producatorului, intervalul in care se incadreaza numerele produse, adica [min,max], o instanta a clasei 
 *  PCLock si un contor care numara cate elemente au fost produse. Metodele implementate sunt:
 *  <p>@link #Producer()- constructorul clasei unde se fac initializari</p>
 *  <p>@link #getCounter() -get care returneaza cat a produs fiecare producator</p>
 *  <p>@link #run()</p>
 * 
 * @author Marinescu Ana
 *
 */

public class Producer extends Thread {  
	
	int npthreads;			
	int id;					
	int max;				
	int min;				
	PCLock buffer;    	
	int contor = 0;       // Contor de elemente trimise   
	
	/**
	 * Se initilaizeaza urmatoarele variabile:
	 * @param npthreads  numarul de producatori
	 * @param id  id-ul producatorului
	 * @param max  cel mai mare numar care poate fi produs
	 * @param min  cel mai mic numar care poate fi produs
	 * @param buffer  instanta a clasei PCLock
	 */
	public Producer(int npthreads, int id, int min, int max, PCLock buffer) {
		
		this.npthreads = npthreads; 
		
		this.id = id;     
		
		this.min = min; 
		
		this.max = max;     
		
		this.buffer = buffer;   
	}
 
	
	/**
	 * @return numarul de elemente care au fost produse
	 */
	int getCounter() 
	{ 
		return contor; 
	}   
	
	/**
	 * <p>int i este un contor pentru a determina valoarea elementului care va fi produs</p>
	 * 
	 * Initial este determinat primul produs, dupa care sunt generate urmatoarele atata timp cat valoarea lor
	 * este mai mica sau egala cu max. Adaugarea lor in buffer se face cu functia append, metoda apratinand clasei
	 * PCLock. Odata ce un produs este adaugat in buffer, se incrementeaza  variabila care tine evidenta numarului
	 * de valori produse.
	 */
	public void run() {     

		int i = min;     
		
		while ((i % npthreads) != id) { 
			
			i++; 
			
		} 
		
		while (i <= max) {       
			
			buffer.append(i); 
			
			contor++;
			
			System.out.println("Producer "+id+" produced "+i);
			
			i += npthreads;
			
		}
		
	}
	
} 
	
