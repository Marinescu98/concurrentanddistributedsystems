package Semaphores;


import java.util.concurrent.Semaphore;

/**
 *  <strong>PCSemphore</strong>
 *  Metodele clasei sunt urmatoarele:
 *  <p>@link #append() </p>
 *  <p>@link #take()</p>
 * 	
 * <p>Membrii de tip data sunt urmatorii:</p>
 * <p>length_buffer - reprezinta lungimea bufferului</p>
 * <p>buffer[] - pastreaza elementele </p>
 * <p>count - numara elementele din buffer</p>
 * <p>oldest, newest - reprezinta pozitiile elementelor</p>
 * <p>s_consumer - instanta a clasei PCSemaphore corespunzatoare consumatorilor </p>
 * <p>s_producer - instanta a clasei PCSemaphore corespunzatoare producatorilor</p>
 * 
 * @author Marinescu Ana
 *
 */

public class PCSemaphore 
{   
	private static Semaphore s_consumer=new Semaphore(0); 	
	private static Semaphore s_producer=new Semaphore(1); 
	
	final int length_buffer = 5;   	
	int oldest = 0, newest = 0;  
	volatile int count = 0;   	
	int buffer[] = new int[length_buffer];   
	
	/**
	 * Cat timp bufferul e plin, producatorul asteapta ca un element sa fie consumat prin apelarea functiei acquire, iar odata ce e 
	 * spatiu  disponibil se adauga v in buffer si se incrementeaza numarul de elemente din buffer. Apoi se apeleaza release
	 * pentru anunta ca bufferul e din nou plin.
	 * 
	 * @param v elementul generat pentru a fi adaugat in buffer
	 */
	void append(int v) {  

		while (count == length_buffer) {       
			
			try {         
				
				s_producer.acquire();     
				
			} catch (InterruptedException e){
				
				e.printStackTrace();
				
			}     
			
		}     
		
		buffer[newest] = v;     
		
		newest = (newest + 1) % length_buffer;     
		
		count++;     
		
		s_consumer.release();
		
	} 
			
	/**
	 * Cat timp bufferul e gol, consumatorul asteapta un nou element de consumat, iar odata ce e 
	 * adaugat, este consumat si se decrementeaza numarul de elemente din buffer. Apoi se apeleaza release
	 * pentru anunta producatorul ca bufferul e gol si poate produce din nou si apoi se returneaza elementul consumat.
	 * 
	 * @return elementul consumat
	 */
	int take() {     
		
		int temp;     
	
		while (count == 0) {       
		
			try {         
			
				s_consumer.acquire();     
			
			} catch(InterruptedException e){
				
				e.printStackTrace();
				
			}     
		
		}     
	
		temp = buffer[oldest];     
	
		oldest = (oldest + 1) % length_buffer;     
	
		count--;     
	
		s_producer.release();
	
		return temp;   
	
	} 

} 