package Semaphores;

import java.util.Random;
import java.util.Scanner;

/**
 * <strong>Main</strong>
 * <p>
 * Clasa implementeaza metoda  @link #main() 
 * </p>
 *
 * @author Marinescu Ana
 *
 */
public class Main {
		
	static int MINN=1;   	
	static int MAXN=0;   	
	static int NPTHREADS;	
	static int NCTHREADS;
	
	static Producer pThreads[];
	static Consumer cThreads[];
	 
	static PCSemaphore buffer = new PCSemaphore();
	
	public static int generateRandomNumber(int max) {
		Random random = new Random();
		return random.nextInt(max);
	}

	/**
	 *<p> In functia main initial se citesc datele de la tastatura in doua moduri: fie se da o limita pentru un interval [0,n] unde n e numarul dat la intrare si 
	 * apoi se genereaza random valorile variabilelor statice NPTHREADS, NCTHREADS, MINN si MAXN, fie sunt date de la tastatura toate pentru. Dupa citirea datelor
	 * de la tastatura si initializarea variabilelor se defineste lungimea celor doi vectori pThreads si cThreads.</p>
	 * <p>Urmeaza initializarea threadurilor pThreads apeland constructorul corespunzator clasei Producer si punerea lor in executie prin apelarea metodei start(). 
	 * Acelasi lucru se intampla si in cazul threadurilor cThreads. Dupa ce se parcurg toti vectorii, urmeaza suspendarea executiei lor prin apelarea metodei join.
	 * Apoi, pentru fiecare consumator se afiseaza  numarul de elemente consumate si pentru fiecare producator se afiseaza numarul de elemente produse.</p>
	 * <p>Variabilele necesare sunt:</p>
	 * <p>NPTHREADS - numarul de threaduri producator </p>
	 * <p>NCTHREADS - numarul de threaduri consumator</p>
	 * <p>pThreads - vectorul de threaduri producator</p>
	 * <p>cThreads - vectorul de threaduri consumator</p>
	 * <p>buffer - obiect de tip PCSemaphore</p>
	 * <p>MINN, MAXN - plaja de valori</p>
	 * <p>LIMIT - limita pentru intervalul [0,LIMIT]</p>
	 * <p>i - contor		</p>
	 *  
	 *  @param args	args
	**/
	public static void main(String[] args) { 
		
		int i;    
		
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Type 1 if you want to generate random numbers or other number if you want to add yourself:");
		
		if(sc.nextInt()==1)
		{
			System.out.println("Type the limits: ");
			final int LIMIT=sc.nextInt();
			
			while(MINN>MAXN)
			 {
				MINN=generateRandomNumber(LIMIT);   			
				MAXN=generateRandomNumber(LIMIT);   
			 }
			 NPTHREADS=generateRandomNumber(LIMIT);			
			 NCTHREADS=generateRandomNumber(LIMIT);
			 
			 System.out.println("[MINN, MAXN] -> ["+MINN+","+MAXN+"]");
			 System.out.println("NPTHREADS = "+NPTHREADS);
			 System.out.println("NCTHREADS = "+NCTHREADS);
		
		}else {
			System.out.println("Type the number of consumers: ");
			 NCTHREADS=sc.nextInt();
			
			System.out.println("Type the number of producers: ");
			 NPTHREADS =sc.nextInt();
			
			System.out.println("Type the min value of elements: ");
			 MINN=sc.nextInt();   
			
			System.out.println("Type the mac value of elements: ");
			 MAXN=sc.nextInt();  
		
		}
		pThreads = new Producer[NPTHREADS];   
		cThreads = new Consumer[NCTHREADS]; 
		
		System.out.println("Producer - Consumer started");
							
		for (i=0;i<NPTHREADS;i++) {       
			pThreads[i] = new Producer(NPTHREADS, i, MINN, MAXN, buffer);       
			pThreads[i].start();  
		}
			
		for (i=0;i<NCTHREADS;i++) {       
			cThreads[i] = new Consumer(NCTHREADS, i, MINN, MAXN, buffer);       
			cThreads[i].start();     
		}     

		for (i=0;i<NPTHREADS;i++) {       
			try { 
				pThreads[i].join(); 
			}
			catch(InterruptedException e) {}     
		}     
				
		for (i=0;i<NCTHREADS;i++) {      
			try { 
				cThreads[i].join(); 
			}       
			catch(InterruptedException e) {}     
		}     
					
		for (i=0; i<NPTHREADS; i++) {       
			System.out.println("Producer "+i+" produced "+pThreads[i].getCounter()+" elements.");     
		}     
						
		for (i=0; i<NCTHREADS; i++) {       
			System.out.println("Consumer "+i+" consumed "+cThreads[i].getCounter() +" elements.");   			
		}  
				
		System.out.println("Producer - Consumer finished"); 
	}
			
} 
				
