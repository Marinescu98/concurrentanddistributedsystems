package Monitor;

/**
 *  <strong>PCMonitor</strong>
 *  Clasa implementeaza un monitor. Metodele clasei sunt urmatoarele:
 *  <p>@link #append(...) </p>
 *  <p>@link #take()</p>
 * 	
 *<p> Membrii de tip data sunt urmatoarii:</p>
 * <p>length_buffer - reprezinta lungimea bufferului</p>
 * <p>buffer[] - pastreaza elementele </p>
 *<p> count - numara elementele din buffer</p>
 * <p>oldest, newest - reprezinta pozitiile elementelor</p>
 * 
 * @author Marinescu Ana
 *
 */

public class PCMonitor 
{   
	final int length_buffer = 5;   
	
	int oldest = 0;
	
	int newest = 0;  
	
	volatile int count = 0;   
	
	int buffer[] = new int[length_buffer];   

	/**
	 * Functia primeste ca parametru elementul generat de producator, si se asteapta folosind wait,
	 * pana cand se elibereaza vreo pozitie in buffer. Dupa ce se elibereaza vreo pozitie,
	 *  elementul este adaugat si creste nr de elemente din buffer incrementandu-se contorul. 
	 * Dupa adaugarea in buffer, se apeleaza functia notifyAll() anunta toate theadurile ca bufferul
	 * adre din nou elemente in el.
	 * @param v elementul generat de producator
	 */
	synchronized void append(int v) {     
		
		while (count == length_buffer) {       
		
			try {         
				wait();       
				} catch (InterruptedException e){}     
			
		}     
		
		buffer[newest] = v;     
		
		newest = (newest + 1) % length_buffer;     
		
		count++;     
		
		notifyAll();  
		
	} 
			
	/**
	 * Se asteapta pana ce bufferul are macar un element folosind wait, iar odata ce este
	 * adaugat un element in buffer, se iese din while. Elementul este extras din buffer si
	 * atribuit variabilei temporale temp. Scade numarul de elemente din buffer, apoi prin intermediul
	 * functiei notifyAll() se anunta toate threadurile ca bufferul e disponibil pentru a se adauga din nou in el,
	 * iar elementul consumat este returnat.
	 * 
	 * @return elementul consumat
	 */
	synchronized int take() { 
		
		int temp;     
		
		while (count == 0) {       
			try {         
				wait();     
				} catch(InterruptedException e){}     

		}     
		
		temp = buffer[oldest];     
		
		oldest = (oldest + 1) % length_buffer;     
		
		count--;     
		
		notifyAll();    
		
		return temp;   
		
	} 
} 
