package Monitor;

/**
 *  <strong>Consumer</strong>
 *  Consumer extinde clasa Thread si suprascrie metoda run(). Constructorul primeste ca parametrii numarul de threaduri consumator, nrthreads,
 *  id-ul consumatorului, intervalul in care se incadreaza numerele consumate, adica [min,max], o instanta a clasei 
 *  PCMonitor denumita buffer si un contor care numara cate elemente au fost consumate. Metodele implementate sunt:
 *  <p>@link #Producer(...) - constructorul clasei unde se fac initializari</p>
 *  <p>@link #getCounter() - get care returneaza numarul de elemente care au fost consumate</p>
 *  <p>@link #run() - implementeaza functionalitatea clasei </p>
 * 
 * @author Marinescu Ana
 */

public class Consumer extends Thread {   
	
	int ncthreads;		
	int id;				
	int max;			
	int min;			
	PCMonitor buffer;  	
	int contor = 0;    	// contor de elemente consumate 
	
	/**
	 * Se initilaizeaza urmatoarele variabile:
	 * @param ncthreads  numarul de consumatori
	 * @param id  id-ul consumatorului
	 * @param max  cel mai mare numar care poate fi consumat
	 * @param min  cel mai mic numar care poate fi consumat
	 * @param buffer  instanta a clasei PCMonitor
	 */
	public Consumer(int ncthreads, int id, int min, int max, PCMonitor buffer) {   
	
		this.ncthreads = ncthreads; 
		
		this.id = id;     
		
		this.min = min; 
		
		this.max = max;     
		
		this.buffer = buffer;   
	}   
	
	/**
	 * @return numarul de elemente care au fost consumate
	 */
	int getCounter() { 
		return contor; 
	}   
	
	/**
	 * <p>int i - contor folosit pentru generarea elementelor de consumat</p>
	 * 
	 * <p>Contorului i este incrementat pana pana ajunge la prima valorare, adica pana cand impartirea lui la
	 * npthreads da un numar diferit de id. Cat timp i este mai mic ca max, se genereaza un element 
	 * apeland metoda take implementata in clasa PCMonitor.</p>
	 * <p>Valoarea returnata de functie este contorizata si afisata, apoi creste contorul i cu npthreads.
	 * De asemenea, pe langa contorul i, mai este incrementat si contor, care numara cate elemente au fost consumate.</p>
	 */
	public void run() {    
		
		int i = min;
		
		int was_consumed; 
		
		while ((i % ncthreads) != id) {
			i++; 
			}     
		
		while (i <= max) {       
		
			was_consumed = buffer.take(); 
			
			contor++;      
			
			System.out.println("Consumer "+id+" consumed "+was_consumed);      
			
			i += ncthreads;    
			
		}   		
	} 
	
} 
