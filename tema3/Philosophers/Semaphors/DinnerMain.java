package Semaphors;

import java.util.Random;
import java.util.Scanner;

/**
 * <strong>DinnerMain</strong>
 * <p>Clasa implementeaza metoda  @link #main()</p>
 * <p>Clasa implementeaza metoda  @link #generateRandomNumber() pentru generarea unor numere aleatoare</p>
 *
 * @author Marinescu Ana
 *
 */
	public class DinnerMain {
		
		static int NUMBER_OF_PHILOSOPHERS_AND_FORKS;
		static int FOOD_LEFT;
		
		public static int generateRandomNumber(int max) {
			
			Random random = new Random();
			
			return random.nextInt(max);
		}
		
		/**
		 *<p> In functia main initial se citesc datele de la tastatura in doua moduri: fie se da o limita pentru un interval [0,n] unde n e numarul dat la intrare si 
		 * apoi se genereaza random valorile variabilelor statice NUMBER_OF_PHILOSOPHERS_AND_FORKS si FOOD_LEFT, fie sunt date de la tastatura pentru toate. Dupa citirea datelor
		 * de la tastatura si initializarea variabilelor se definesc cei doi vectori philosopher si forks.</p>
		 * <p>Urmeaza initializarea vectorului forks apeland constructorul corespunzator clasei Fork si vectorului philosophers apeland constructorul din 
		 * clasa Philosopher si crearea threadurilor pthreads si punerea lor in executie prin apelarea metodei start(). Dupa ce se parcurg toti vectorii, urmeaza suspendarea executiei lor prin apelarea metodei join.
		 * </p>
		 * <p>Variabilele necesare sunt:</p>
		 * <p>NUMBER_OF_PHILOSOPHERS_AND_FORKS - numarul de filosofi si furculite </p>
		 * <p>FOOD_LEFT - cantitatea de mancare disponibila pentru toti filosofii</p>
		 * <p>philosophers - vectorul de threaduri Philosopher</p>
		 * <p>forks - vectorul de threaduri Fork</p>
		 * <p>i - contor</p>
		 * <p>threads - vectorul de threaduri</p>
		 *  
		 *  @param args	args
		**/
		public static void main(String[] args) throws  Exception{
	       
			Scanner sc=new Scanner(System.in);
			
			System.out.println("Type 1 if you want to generate random numbers or other number if you want to add yourself:");
			
			if(sc.nextInt()==1)
			{
				System.out.println("Type the limits: ");
				
				int limits=sc.nextInt();
				
				NUMBER_OF_PHILOSOPHERS_AND_FORKS=generateRandomNumber(limits);
				
				FOOD_LEFT=generateRandomNumber(limits);
				
				 System.out.println("NUMBER_OF_PHILOSOPHERS_AND_FORKS = "+NUMBER_OF_PHILOSOPHERS_AND_FORKS);
				 
				 System.out.println("FOOD_LEFT = "+FOOD_LEFT);
				
			}else {
				
				System.out.println("Type the number of forks and philosophers: ");
				
				NUMBER_OF_PHILOSOPHERS_AND_FORKS=sc.nextInt();
				
				System.out.println("Type the food that the philosopher can eat: ");
				
				FOOD_LEFT=sc.nextInt();
			}
			
			Philosopher[] philosophers = new Philosopher[NUMBER_OF_PHILOSOPHERS_AND_FORKS];       
			Fork[] forks = new Fork[NUMBER_OF_PHILOSOPHERS_AND_FORKS];
	        Thread[] threads=new Thread[NUMBER_OF_PHILOSOPHERS_AND_FORKS+1];
	        
			System.out.println("Dinner is starting!");
	        
	        for(int i=0;i<NUMBER_OF_PHILOSOPHERS_AND_FORKS;i++){
            
	            forks[i]=new Fork("left");
	           
	            if(i+1!=NUMBER_OF_PHILOSOPHERS_AND_FORKS)
	            	{
	            		forks[i+1]=new Fork("right");
	            	}
	            
	            if(i+1 != NUMBER_OF_PHILOSOPHERS_AND_FORKS){
	                {
	                	philosophers[i] = new Philosopher(i,FOOD_LEFT,forks[i],forks[i+1]);
	                }
	            }else{
	                philosophers[i] = new Philosopher(i,FOOD_LEFT,forks[i-1],forks[i]);
	            }
	            
	            threads[i] = new Thread(philosophers[i]);
	            
	            threads[i].start();

	        }
	        
			for(int i = 0; i < NUMBER_OF_PHILOSOPHERS_AND_FORKS; i++)
				try {
					threads[i].join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			System.out.println("Dinner is over!");	
	    }
	
	}

