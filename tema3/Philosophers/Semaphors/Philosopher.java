package Semaphors;

/**
 *  <strong>Philosopher</strong>
 *  Philosopher extinde clasa Thread si suprascrie metoda run(). Metodele implementate sunt:
 *  <p>@link #Philosopher(...) - constructorul clasei unde se fac initializari</p>
 *  <p>@link #think() - metoda folosita pentru a evidentia momentul in care un filosof gandeste</p>
 *  <p>@link #eat() - metoda folosita pentru a evidentia momentul in care un filosof mananca</p>
 *  <p>@link #run() - implementeaza functionalitatea clasei </p>
 * 
 * @author Marinescu Ana
 */
public class Philosopher implements Runnable{
    private Fork leftFork;
    private Fork rightFork;
    private int foodLeft;
    private int id;
    
	/**
	 * Se initilaizeaza urmatoarele variabile:
	 * @param foodLeft  cantitatea de mancare disponibila 
	 * @param id id-ul filosofului
	 * @param leftFork furculita din stanga filosofului
	 * @param rightFork furculita din dreapta filosofului
	 */
    public Philosopher(int id,int foodLeft, Fork leftFork,Fork rightFork){
    	this.id=id;
        this.leftFork= leftFork;
        this.rightFork= rightFork;
        this.foodLeft=foodLeft;
    }

	/**
	 * In functie, initial toti filosofii gandesc, si cat timp exista mancare disponibila, daca cele doua furculite sunt disponibile atunci mananca, ceea ce inseamna 
	 * ca se decrementeaza cantitatea de mancare si dupa ce termina lasa furculitele jos, apoi se intorc la gandit.
	 */
    public void run() {
  
        try{
      	  while(foodLeft>0){
            	
      		  System.out.println("Philosopher "+id+" is thinking");
            	
      		  Thread.sleep((int)(Math.random()*100));

      		  if(leftFork.take(id)){
    						
    				if(rightFork.take(id)){
    						            
    					try {
    						            	
    							System.out.println("Philosopher "+id+" eats");
    						            	
    							this.foodLeft-=1;
    						            	
    							Thread.sleep((int)(Math.random()*100));
    						            
    					} catch (InterruptedException ex) { 
    						    return;	            
    					}
    						            
    					leftFork.putDown(id);
    					rightFork.putDown(id);

    				}
    			}else{
    		       	leftFork.putDown(id);
    			} 				
      	  }   		
        }catch(InterruptedException e){
            Thread.currentThread().interrupt();
            return;
        }
    }
}