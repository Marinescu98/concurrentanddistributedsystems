package Semaphors;

import java.util.concurrent.Semaphore;

/**
 *  <strong>Fork</strong>
 *	<p>Fork extinde clasa Thread si suprascrie metoda run() </p>
 * 	<p> Metodele implementate sunt:</p>
 * 	<p>@link #Fork(...) - constructorul clasei unde se fac initializari</p>
 *  <p>@link #PickUp() - functie apelata in momentul cand filosoful al carui id este primit ca parametru, ridica fruculita</p>
 *  <p>@link #PutDown() - functie apelata in momentul cand filosoful al carui id este primit ca parametru, pune furculita jos </p>
 * @author Marinescu Ana
 */
class Fork {

	public  Semaphore fork = new Semaphore(1);
	public String type;

	/**
	 * <p>Se initilaizeaza variabila type care retine pozitia furculite (true daca e luata sau false daca e lasata jos)</p>
	 *
	 * @param type pozitia furculitei raporatata la filosof (adica dreapta sau stanga)
	 */
	Fork(String type)
	{
		this.type=type;
	}

	/**
	 * Functia este apelata in momentul in care un filosof al carui id este primit ca parametru vrea sa manance.
	 * @param id_philosopher idul filosofului care a luat furculita
	 * @return returneaza daca furculita poate sau nu sa fie luata
	 */
	public boolean take(int id_philosopher) {
		System.out.println("Philosopher "+id_philosopher+" picked up "+type+ " fork");
		return fork.tryAcquire();
	}

	/**
	 * Functia este apelata in momentul in care un filosof al carui id este primit ca parametru, pune furculita jos. 
	 * 
	 * @param idPhilosopher idul filosofului care a luat furculita
	 */
	public void putDown(int id_philosopher) {
		System.out.println("Philosopher "+id_philosopher+" puts down "+type+ " fork");
		fork.release();
	}
}