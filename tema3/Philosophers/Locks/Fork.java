package Locks;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *  <strong>Fork</strong>
 *	<p>Fork extinde clasa Thread si suprascrie metoda run(). Constructorul primeste ca parametrii id-ul filosofului si pozitia furculite(adica dreapta sau stanga) </p>
 * 	<p> Metodele implementate sunt:</p>
 * 	<p>@link #Fork(...) - constructorul clasei unde se fac initializari</p>
 *  <p>@link #PickUp() - functie apelata in momentul cand filosoful al carui id este primit ca parametru, ridica fruculita</p>
 *  <p>@link #PutDown() - functie apelata in momentul cand filosoful al carui id este primit ca parametru, pune furculita jos </p>
 * @author Marinescu Ana
 */

class Fork {

	volatile Boolean  isTake;
	static Lock lock;

	/**
	 * <p>Se initilaizeaza urmatoarele variabile:</p>
	 * <p>lock -  instanta a clasei ReentrantLock folosita pentru gestionarea zavorului</p>
	 * <p>isTake - variabila care retin starea furculite (true daca e luata sau false daca e lasata jos)</p>
	 */
	Fork(){
		
		lock = new ReentrantLock();
		
		isTake = false;
	}

	/**
	 * Functia este apelata in momentul in care un filosof al carui id este primit ca parametru, ridica furculita ca sa manance. Initial zavorul este inchis pentru
	 * a nu intra alte fire peste el, schimbam starea furcultiei ca sa marcam ca a fost luata si o returnam
	 * @param idPhilosopher idul filosofului care a luat furculita
	 * @param type pozitia furculitei raporata la filosof (adica dreapta sau stanga)
	 * @return returneaza starea furculitei
	 */
	public Boolean PickUp(int idPhilosopher, String type){

			lock.lock();
		
			isTake=true;
			
			System.out.println("Philosopher "+idPhilosopher+" picks up "+type+" fork");
			
			return isTake;
	}
		
	/**
	 * Functia este apelata in momentul in care un filosof al carui id este primit ca parametru, pune furculita jos. Dupa ce schimbam starea furculitei pentru a 
	 * arata ca e disponibila, deschidem zavorul pentru ca alt thread sa poata intra
	 * @param idPhilosopher idul filosofului care a luat furculita
	 * @param type pozitia furculitei raporata la filosof (adica dreapta sau stanga)
	 */
	public void PutDown(int idPhilosopher, String type){
		
		try{
			
			isTake=false;
			
			System.out.println("Philosopher "+idPhilosopher+" puts down "+type+" fork");
		
		}
		finally {
			
			lock.unlock();
		
		}
	}		
}