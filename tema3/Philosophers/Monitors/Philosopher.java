package Monitors;



/**
 *  <strong>Philosopher</strong>
 *  Philosopher extinde clasa Thread si suprascrie metoda run(). Metodele implementate sunt:
 *  <p>@link #Philosopher(...) - constructorul clasei unde se fac initializari</p>
 *  <p>@link #think() - metoda folosita pentru a evidentia momentul in care un filosof gandeste</p>
 *  <p>@link #eat() - metoda folosita pentru a evidentia momentul in care un filosof mananca</p>
 *  <p>@link #run() - implementeaza functionalitatea clasei </p>
 * 
 * @author Marinescu Ana
 */
public class Philosopher extends Thread {

    private final Fork leftFork; 
    private final Fork rightFork;   
    private volatile static int foodLeft; 
    private int id;
    
	/**
	 * Se initilaizeaza urmatoarele variabile:
	 * @param foodLeft  cantitatea de mancare disponibila 
	 * @param id id-ul filosofului
	 * @param leftFork furculita din stanga filosofului
	 * @param rightFork furculita din dreapta filosofului
	 */
    public Philosopher(int id, int foodLeft, Fork leftFork, Fork rightFork) {
        this.leftFork = leftFork;
        this.rightFork = rightFork;
        this.id=id;
        this.foodLeft=foodLeft;
    }
   
	/**
	 * In functie, initial toti filosofii gandesc, si cat timp exista mancare disponibila, daca cele doua furculite sunt disponibile atunci mananca si dupa ce termina
	 * lasa furculitele jos, apoi se intorc la gandit.
	 */
	public void run() {
		
		think();
		
		while( foodLeft > 0 ){
			
			if(leftFork.PickUp(id,"left")==true);
			{
				if(rightFork.PickUp(id,"right")==true)
				{
					eat();
			
					rightFork.PutDown(id,"right");
				}
				
				leftFork.PutDown(id,"left");
			}
			
			think();
		}		
	}
	
	/**
	 * Functia semnaleaza faptul ca filosoful care a apelat-o gandeste, iar momentul cat acesta gandeste este dat de functia sleep implementata in clasa Thread.
	 */
	void think() {
		System.out.println("Philosopher "+id+" think ");
		
		try {
			Thread.sleep((int)(Math.random()*100));
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Functia semnaleaza faptul ca filosoful care a apelat-o mananca, ceea ce inseamna ca se decrementeaza cantitatea de mancare, iar durata momentul cat acesta 
	 * mananca este dat de functia sleep implementata in clasa Thread.
	 */
	void eat(){
		
		System.out.println("Philosopher "+id+" eats ");
		
		foodLeft--;
		
		try {
			Thread.sleep((int)(Math.random()*100));
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}