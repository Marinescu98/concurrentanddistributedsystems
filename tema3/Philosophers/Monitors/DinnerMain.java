package Monitors;

import java.util.Random;
import java.util.Scanner;

	
/**
 * <strong>DinnerMain</strong>
 * <p>Clasa implementeaza metoda  @link #main()</p>
 * <p>Clasa implementeaza metoda  @link #generateRandomNumber() pentru generarea unor numere aleatoare</p>
 *
 * @author Marinescu Ana
 *
 */
	
	
	public class DinnerMain {
	
		static int NUMBER_OF_PHILOSOPHERS_AND_FORKS;
		static int FOOD_LEFT;
		
		public static int generateRandomNumber(int max) {
			Random random = new Random();
			return random.nextInt(max);
		}
		
		/**
		 *<p> In functia main initial se citesc datele de la tastatura in doua moduri: fie se da o limita pentru un interval [0,n] unde n e numarul dat la intrare si 
		 * apoi se genereaza random valorile variabilelor statice NUMBER_OF_PHILOSOPHERS_AND_FORKS si FOOD_LEFT, fie sunt date de la tastatura pentru toate. Dupa citirea datelor
		 * de la tastatura si initializarea variabilelor se definesc cei doi vectori philosopher si forks.</p>
		 * <p>Urmeaza initializarea threadurilor forks apeland constructorul corespunzator clasei Fork si threadurile philosophers apeland constructorul din 
		 * clasa Philosopher si punerea lor in executie prin apelarea metodei start(). Dupa ce se parcurg toti vectorii, urmeaza suspendarea executiei lor prin apelarea metodei join.
		 * </p>
		 * <p>Variabilele necesare sunt:</p>
		 * <p>NUMBER_OF_PHILOSOPHERS_AND_FORKS - numarul de filosofi si furculite </p>
		 * <p>FOOD_LEFT - cantitatea de mancare disponibila pentru toti filosofii</p>
		 * <p>philosophers - vectorul de threaduri Philosopher</p>
		 * <p>forks - vectorul de threaduri Fork</p>
		 * <p>i - contor</p>
		 *  
		 *  @param args	args
		**/
		public static void main(String[] args){
			
			Scanner sc=new Scanner(System.in);
			
			System.out.println("Type 1 if you want to generate random numbers or other number if you want to add yourself:");
			
			if(sc.nextInt()==1)
			{
				System.out.println("Type the limits: ");
				
				int limits=sc.nextInt();
				
				NUMBER_OF_PHILOSOPHERS_AND_FORKS=generateRandomNumber(limits);
				
				FOOD_LEFT=generateRandomNumber(limits);
				
				System.out.println("NUMBER_OF_PHILOSOPHERS_AND_FORKS = "+NUMBER_OF_PHILOSOPHERS_AND_FORKS);
				 
				System.out.println("FOOD_LEFT = "+FOOD_LEFT);
				
			}else {
				
				System.out.println("Type the number of forks and philosophers: ");
				
				NUMBER_OF_PHILOSOPHERS_AND_FORKS=sc.nextInt();
				
				System.out.println("Type the food that the philosopher can eat: ");
				
				FOOD_LEFT=sc.nextInt();
			}
			
			Philosopher[] philosophers = new Philosopher[NUMBER_OF_PHILOSOPHERS_AND_FORKS];
	        
			Fork[] forks = new Fork[NUMBER_OF_PHILOSOPHERS_AND_FORKS];
			
			System.out.println("Dinner is starting!");

			for(int i = 0; i < NUMBER_OF_PHILOSOPHERS_AND_FORKS; i++)
				{
					forks[i] = new Fork();
				}
			
			for(int i = 0; i < NUMBER_OF_PHILOSOPHERS_AND_FORKS; i++)
				{
					if(i==0)
						{
							philosophers[i] = new Philosopher(i, FOOD_LEFT, forks[NUMBER_OF_PHILOSOPHERS_AND_FORKS-1],forks[0]);
						}
					else {
						philosophers[i]=new Philosopher(i, FOOD_LEFT, forks[i], forks[i-1]);			
					}
				}
			for(int i = 0; i < NUMBER_OF_PHILOSOPHERS_AND_FORKS; i++)
				 {
					philosophers[i].start();
				 }
				

			for(int i = 0; i < NUMBER_OF_PHILOSOPHERS_AND_FORKS; i++)
				{
					try{
						philosophers[i].join();			
					}catch (Exception e) {
						e.printStackTrace();
					}
				}

			System.out.println("Dinner is over!");	

	    }
	
	}

