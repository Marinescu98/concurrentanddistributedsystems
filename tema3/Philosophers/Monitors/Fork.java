package Monitors;

/**
 *  <strong>Fork</strong>
 *	<p>Fork extinde clasa Thread si suprascrie metoda run() </p>
 * 	<p> Metodele implementate sunt:</p>
 * 	<p>@link #Fork(...) - constructorul clasei unde se fac initializari</p>
 *  <p>@link #PickUp() - functie apelata in momentul cand filosoful al carui id este primit ca parametru, ridica fruculita</p>
 *  <p>@link #PutDown() - functie apelata in momentul cand filosoful al carui id este primit ca parametru, pune furculita jos </p>
 * @author Marinescu Ana
 */

class Fork {

	private Boolean isTake;

	/**
	 * <p>Se initilaizeaza variabila isTake care retin starea furculite (true daca e luata sau false daca e lasata jos)</p>
	 */
	Fork(){
		isTake = false;
	}

	/**
	 * Functia este apelata in momentul in care un filosof al carui id este primit ca parametru, ridica furculita ca sa manance. Initial se asteapta pana cand furculita poate
	 * fi luata de filosoful curent, folosind metoda wait. Cand furculita este disponibila, se marcheaza din nou ca fiind ocupata si starea ei este returnata.
	 * @param idPhilosopher idul filosofului care a luat furculita
	 * @param type pozitia furculitei raporata la filosof (adica dreapta sau stanga)
	 * @return returneaza starea furculitei
	 */
	public synchronized Boolean PickUp(int idPhilosopher, String type){
		
		while(isTake)
		{		
			try {
				wait();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
			isTake=true;
			
			System.out.println("Philosopher "+idPhilosopher+" picks up "+type+" fork");
			
			return isTake;
	}
		
	/**
	 * Functia este apelata in momentul in care un filosof al carui id este primit ca parametru, pune furculita jos. Dupa ce schimbam starea furculitei pentru a 
	 * arata ca e disponibila, se notifica restul threadurilor ca furculita poate fi luata.
	 * @param idPhilosopher idul filosofului care a luat furculita
	 * @param type pozitia furculitei raporata la filosof (adica dreapta sau stanga)
	 */
	public synchronized void PutDown(int idPhilosopher, String type){
		
		isTake=false;
			
		System.out.println("Philosopher "+idPhilosopher+" puts down "+type+" fork");
		
		notifyAll();
	}		
}