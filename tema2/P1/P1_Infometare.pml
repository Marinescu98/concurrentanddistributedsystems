/*declararea si implementarea variabilelor globale*/
bool    wantp = false, wantq = false;
byte    turn = 1;

/*variabilele globale booleene folosite pentru verificarea lipsei infometarii*/
bool x=false, y=false;

/*formula ltl care descrie lipsa infometarii*/
ltl{[]<>x && []<>y}

/*primul proces*/
active proctype p() {
/*sectiunea non-critica a buclei while infinite*/
    do
    ::  wantp = true;
        do
        :: wantq ->
            if
            :: (turn == 2) ->
                wantp = false;
                turn == 1;
                wantp = true
			::else
            fi
		::else->break;
        od;

		/*sectiunea critica a buclei while infinite*/
        printf("P se afla in sectiunea critica\n");

		/*verificarea lipsei infometraii cu cele doua variabile booleene*/
		x=true; 
		assert(!(x&&y)); 
		x=false;

 		turn = 2;
		wantp = false
    od
}

/*al doilea proces proces*/
active proctype q() {
/*sectiunea non-critica a buclei while infinite*/
     do
    ::  wantq = true;
        do
        :: wantp ->
            if
            :: (turn == 1) ->
                wantq = false;
                turn == 2;
                wantq = true
			::else
            fi
		::else->break;
        od;

		/*sectiunea critica a buclei while infinite*/
        printf("Q se afla in sectiunea critica\n");

		/*verificarea lipsei infometraii cu cele doua variabile booleene*/
		y=true; 
		assert(!(x&&y)); 
		y=false;

		turn = 1;
        wantq = false       
    od
}

