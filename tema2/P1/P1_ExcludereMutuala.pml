/*declaratii si initializari ale variabilelor globale*/
bool    wantp = false, wantq = false;
byte    turn = 1;

/*variabila globala folosita pentru a verifica excluderea mutuala*/
int critical=0;

/*formula ltl care descrie excluderea mutuala*/
ltl{[](critical<=1)}

/*primul proces*/
active proctype p() {
/*sectiunea non-critca a buclei infinite while*/
    do
    ::  wantp = true;
        do
        :: wantq ->
            if
            :: (turn == 2) ->
                wantp = false;
                turn == 1;
                wantp = true
			::else
            fi
		::else->break;
        od;

		/*sectiunea critca a buclei infinite while*/
        printf("P se afla in sectiunea critica\n");
		
		/*verificarea excluderii mutuale prin incrementarea si decrementarea variabilei critical*/
		critical++; 
		assert(critical <= 1); 
		critical--;

		turn = 2;
		wantp = false      
    od
}

/*al doilea proces*/
active proctype q() {
/*sectiunea non-critca a buclei infinite while*/
     do
    ::  wantq = true;
        do
        :: wantp ->
            if
            :: (turn == 1) ->
                wantq = false;
                turn == 2;
                wantq = true
			::else
            fi
		::else->break;
        od;

		/*sectiunea critca a buclei infinite while*/
        printf("Q se afla in sectiunea critica\n");

		/*verificarea excluderii mutuale prin incrementarea si decrementarea variabilei critical*/
		critical++; 
		assert(critical <= 1); 
		critical--;

		turn = 1;
        wantq = false      
    od
}

