/*declaratii variabile globale*/
byte	n = 0; 
byte	finished = 0;

/*implementare procese*/
active proctype P() {

/*i = contor pentru parcurgerea celor 10 pasi
temp = variabila temporala*/

	byte 	i = 1;
	byte	temp;
	do
	:: (i <= 10) -> 
		temp = n;
		n = temp + 1;
		i++
	:: else -> break
	od;
	finished++; 
	printf("n = %d\n", n);
}


active proctype Q() {

/*i = contor pentru parcurgerea celor 10 pasi
temp = variabila temporala*/

	byte 	i = 1;
	byte	temp;
	do
	:: (i <= 10) -> 
		temp = n;
		n = temp + 1;
		i++
	:: else -> break
	od;
	finished++; 
	printf("n = %d\n", n);
}

/*Finish este un proces ce are ca scop semnalarea sfarsitului 
programului: odata ce finished este 2, inseamna ca cele 
doua procese au fost parcurse in intregime
 */
active proctype Finish() {
	finished == 2;  
	printf("n = %d\n", n);
	assert (n > 2);

}