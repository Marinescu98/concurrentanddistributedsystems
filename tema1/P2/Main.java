	/**
	 * <strong>Class Main</strong>
	 * <p>
	 * Clasa data implementeaza pseudocodul dat care presupune declararea a doua fire de executie si rularea lor.
	 * <p>
	 * {@link #main(String[])}
	 * <p>
	 * @author Marinescu Ana
	 * @version 24.10.2019
	 */

	public class Main {
	/**
	 * In main sun create cele doua fire de executie, p si q, prin apelarea constructorului clasei MyThread ce ia ca parametru
	 * numele threadului, adica Q pentru q si P pentru p. De asemenea este apelata functia start ce determina executia instructiunilor
	 * implementate in functia run corespnzatoare clasei, pentru fiecare dintre cele doua threaduri. 
	 * 
	 */
		public static void main(String[] args) {
			
			Thread threadP=new Thread(new MyThread("p"));
			
			Thread threadQ=new Thread(new MyThread("q"));
			
			threadP.start();
			
			threadQ.start();
		}
	
	}
