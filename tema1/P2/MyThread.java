	/**
	 * <strong>Class MyThread</strong>
	 * <p>
	 * Clasa implementeaza interfata Runnable si suprascrie functia {@link #run()}. Membrii de tip data sunt n, care este o variabila
	 * statica, ceea ce inseamna ca va fi comuna tuturor instantelor clasei, dar si volatila si variablia thread care reprezinta
	 * numele threadului.
	 * <p>
	 * {@link #run()}
	 * {@link #MyThread(String)}
	 * <p>
	 *   @author Marinescu Ana
	 *   @since 21.10.2019
	 *   
	 */
	public class MyThread implements Runnable {
	
		private static volatile int n;
	
		private String thread;
		
		/**
		 * MyThread este constructorul care initializeaza numele threadului cu variabila t si pe n cu 0.
		 *  @param t - numele threadului
		 */
		
		public MyThread(String t) {
			
			thread=t;
		
			n=0;
		
		}
		
		/**
		 * In functia run sunt implementate instructiunile date. temp este variabila locala, pentru "do 10 times"
		 * am implementat un for care merge de la 0 la 9. Tot aici este afisat n pentru fiecare thread.
		 */
		
		@Override
		public void run() {
			
			int temp;
			
			for(int i=0;i<10;i++)
			{
			
				temp=n;
				
				n=temp+1;
				
				System.out.println(thread+": n="+n);
			}
			
		}
	
	}
