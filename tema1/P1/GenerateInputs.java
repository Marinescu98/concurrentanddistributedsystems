	import javafx.util.Pair;
	
	/**
	 * <strong>Class GenerateInputs</strong>
	 * <p>
	 * Clasa implementeaza o metoda pentru generarea inputului corespunzator pentru a rula programul, adica a numarului maxim
	 * de numere din intervalul [1,n] si a numarului de threaduri in care trebuie impartit intervalul. Datele returnate nu reprezinta
	 *  un non-trivial input, ci unul care va rula fara probleme.
	 *  <p>
	 * {@link #GenerateInputs()} - functia care genereaza inputul
	 * <p>
	 * @author Marinescu Ana
	 * @version 24.10.2019
	 *
	 */
	
	public class GenerateInputs {
		
		/**
		 * Metoda incepe cu declararea variabilelor locale n si k, unde n reprezinta numarul maxim de numere din intervalul [1,n]
		 * iar k este numarul de fire. Bucla while va rula la infinit, iesirea din ea facandu-se atunci cand variabilele n si k
		 * vor avea valorile potrivite pentru a putea rula programul fara probleme, adica daca n>k si n==k*(n/k)+n%(n/k). Odata
		 * gasite cele doua variabile, se vor returna sub forma unei perechi de numere intregi, ceea ce va generea si iesirea din
		 * loop.
		 * @return perechea de numere care respecta conditia din if: n>k si n==k*(n/k)+n%(n/k)
		 */
		
		public Pair<Integer, Integer> generateNK()
		{
			int n, k;
	
			while(true)
			{
				n=(int)(Math.random()*10000000)+10000;
				
				k=(int)(Math.random()*10000000)+10000;
				
				if(n>k && n==k*(n/k)+n%(n/k))
				{
					return new Pair<Integer, Integer>(n,k);
				}
			}
			
		}
	
	}
