	import java.time.Duration;
	import java.time.Instant;
	import java.util.ArrayList;
	import java.util.List;
	import java.util.Scanner;
	import java.util.Vector;
	import javafx.util.Pair;

	/**
	 * In clasa main este implementata prima problema si respectiv apelate functiile corespunzatoare. Prima problema cere 
	 * implementarea unui program, care sa determine numerele prime dintr-un interval cuprins intre [1,n], 
	 * unde n este un numar citit de la tastatura. Implementarea programului va fi facuta folosind fire (engl. thread), si
	 * problema se va implementa folosind doua metode:<br>
	 * a) prima metoda presupune partitionarea multimii conform unei formule date l_{j}=[(j-1)*q+r+1,j*q+r] unde j este 
	 * 1<=j<=n, n=k*q+r, r este restul impartirii lui n la k si  0<=r<k. In fiecare interval obtinut in urma felierii 
	 * se vor cauta numerele prime.<p>
	 * b) a doua metoda presupune eliminarea multiplilor lui k+1 strict mai mari decat k+1 din multime. Multimea obtinuta
	 * va fi impartita in k submultimi astfel: pentru fiecare j, apartinand multimii [1,k], submultimea j va contine 
	 * doar acele elemente care dau restul j la impartirea prin k+1. De asemenea, se considera k+1 apartinand primei submultimi,
	 * si fiecare fir j va determina numerele prime din multimea lui.
	 * <p>
	 * {@link #Main()} <p>
	 * {@link #SecondMethod(int, int)} - implementeaza a doua metoda<p>
	 * {@link #FirstMethod(int, int)} - implementeaza prima metoda<p>
	 * {@link #Join(Thread[], int)} - apeleaza metoda Join pentru fiecare thread<p>
	 * <p>
	 * @author Marinescu Ana
	 *@version 24.10.2019	 
	 */

	public class Main {

		/**
		 * In functia main sunt initializate variabilele si apelate metodele din clasa Main, adica: {@link #FirstMethod(int, int)},
		 * {@link #SecondMethod(int, int)} si {@link #Join(Thread[], int)}, deja expicate. Primele 5 linii cuprind definitiile 
		 * variabilelor locale: sc care este o variabila de tip Scanner folosita pentru a extrage fluxul de date de la tastatura,
		 * genInt este o instantiere a clasei GenerateInputs folosita pentru a genera un input aleator, choose este folosita pentru a 
		 * naviga prin meniu, n este numarul maxim de numere din intervalul [1,n] si threads_number reprezinta numarul de threaduri.
		 * <p>
		 * Bucla do-while incepe cu afisarea meniului, astfel ca daca la intrare se citeste constanta 1, atunci n si threads_number
		 * sunt introduse de la tastatura, daca se citeste 2, atunci sunt generate de metoda clasei GenerateInputs si daca este 
		 * citit 0 sau oricare numar diferit de 1 si 2 se termina executia buclei.
		 * <p>
		 * Primul if citeste inputul de la tastatura, al doilea il apeleaza prin apelarea metodei care returneaza un pair de tip
		 * Integer, unde primul numar, adica key, e n iar al doilea, adica value, e threads_number. Al treilea if apeleaza metodele
		 * {@link #FirstMethod(int, int)} si {@link #SecondMethod(int, int)} daca <strong>n>threads_number</strong> 
		 * si <strong> n==threads_number*(n/threads_number)+n%(n/threads_number)</strong>, altfel se afiseaza un mesaj corespunztor.
		 * 
		 */
		
		public static void main(String[] args) {
			
			Scanner sc=new Scanner(System.in);
			GenerateInputs genIn=new GenerateInputs();
			int choose=0;
			int n=0;
			int threads_number=0;
			
			do {
				System.out.println();
				System.out.println("0 -> get out of the loop");
				System.out.println("1 -> enter the data from the keyboard");
				System.out.println("2 -> generates numbers randomly");
				
				choose=Integer.parseInt(sc.nextLine());
				
				if(choose==1)
				{
					System.out.print("n = ");
					 
					n=Integer.parseInt(sc.nextLine());
						
					System.out.print("Number of threads = ");
					
					threads_number=Integer.parseInt(sc.nextLine());
						
				}
				else {
					if(choose==2)
					{
						
						Pair<Integer,Integer> pairInput=genIn.generateNK();
						
						n=pairInput.getKey();
						
						threads_number=pairInput.getValue();
						
						System.out.println("\n n = "+n+"\n Number of threads = "+threads_number);
						
					}
				}
				
				if(choose==1||choose==2)
				{
								  		 
					if(n>threads_number && n==threads_number*(n/threads_number)+n%(n/threads_number)){	
					
						System.out.println("\n--------------------METHOD1--------------------------");		 
						
						FirstMethod(n, threads_number);
					 
						System.out.println("\n\n--------------------METHOD2-------------------------");		
					 					 
						SecondMethod(n, threads_number);
						System.out.println("\n n = "+n+"\n Number of threads = "+threads_number);
					}
					else {
					
						System.out.println("The set cannot be divided into "+threads_number+" threads!");
					
					}			
				}
				
			}while(choose!=0);
					
			
		}
		
		/**
		 * In functia SecondMethod, care primeste ca parametru limita superioara a intervalului [1,n] si numarul de threaduri
		 * in care trebuie impartit intervalul. Executia metodei este cronometrata folosind libraria <strong>java.time.Duration<strong>
		 * si <strong>java.time.Instant<strong> astfel: in variabila start2 este memorata timpul de start si in final2 momentul in care 
		 * se termina secventa ce descrie solutia pentru metoda implementata. Cu ajutorul functiei between determin timpul de 
		 * executie a metodei in milisecunde. 
		 * <p>
		 * In primul for se parcurge intervalul [1,n] cu un contor, iterator, si sunt adaugate intr-o ArrayList de numere intregi
		 * doar numere din intervalul specificat anterior, care sunt strict mai mari decat numarul de threaduri(pe care il
		 * consifderam ca fiind k) +1, acest k+1 fiind adaugat la terminarea executiei buclei for, in primul thread.
		 * <p>
		 * Astfel, in array voi avea toate numerele din intervalul [1,n] care respecta conditia deja amintita, urmand ca in a doua
		 * bucla for sa creem threadurile astfel: adaugam pe k+1 in primul thread impreuna cu toate numerele din ArrayListul array
		 * care impartite la k+1 dau restul 1, apoi in al doilea thread voi avea toate numerele care impartite la k+1 dau restul 2
		 *  si tot asa de k ori, la final, in threads vom avea toate firele de executie initializate prin intermediul constructorului
		 *  cu submultimea corespunzatoare.
		 *  <p>
		 *  Dupa ce am initializat cele k threaduri, apelam functia start() ce va rula codul implementat in functia
		 *   {@link #Join(Thread[], int)} pentru fiecare din cele k threaduri, in ordine aleatoare.
		 * 
		 * @param n - limita maxima din intervalul [1,n]
		 * @param threads_number - numarul maxim de threaduri
		 */
			
		public static void SecondMethod(int n, int threads_number)
		{
			List<Integer> array=new ArrayList<Integer>();
			 
			Instant start2=Instant.now();
			 
			for(int iterator=1;iterator<=n;iterator++)
			{ 
				
				if(iterator%(threads_number+1)!=0 && iterator!=threads_number+1)
				{
					array.add(iterator);
				}
			
			}	 		 
				
			Thread[] threads2=new Thread[threads_number+1];
				 
			for(int iterator=1;iterator<=threads_number;iterator++)
			{
				Vector<Integer> subset=new Vector<Integer>();			 
					 	
				for(Integer number : array) {		 
						 
					if(number%(threads_number+1)==iterator)
					{ 
						subset.add(number); 
					}
				}
				
				if(iterator==1)
				{ 
					subset.add(threads_number+1);
				}		 
				
				threads2[iterator]=new Thread(new Method2(subset));
			
			}
				 
			for(int iterator=1;iterator<=threads_number;iterator++)
			{
				threads2[iterator].start();
			}
			
			Instant finish2=Instant.now();				 
			
			Join(threads2,threads_number);
					 
			System.out.println("\n Duration: "+Duration.between(start2, finish2).toMillis()+" milliseconds");
		}
		
		/**
		 * In functia FirstMethod, care primeste ca parametrii limita superioara a intervalului [1,n] si numarul de threaduri
		 * in care trebuie impartit intervalul. Executia metodei este cronometrata folosind libraria <strong>java.time.Duration<strong>
		 * si <strong>java.time.Instant<strong> astfel: in variabila start1 este memorat timpul de start si in final1 momentul in care 
		 * se termina secventa ce descrie solutia pentru metoda implementata. Cu ajutorul functiei between determin timpul de 
		 * executie a metodei in milisecunde. 
		 * <p>
		 * Vectorul threads reprezinta cele k fire de executie care se construiesc astfel: primul thread este initializat separat,
		 * deoarece daca as aplica direct formula pentru un thread j, [(j-1)*repose+rest+1,j*repose+rest], unde repose e catul impartirii
		 * numarului maxim de numere din intervalul [1,n] la numarul total de threaduri, iar rest este restul impartirii lui n la cat, atunci 
		 * pentru rest!=0, numerele din intervalul [1,rest] sunt sarite.
		 * <p>
		 * Astfel, in primul for sunt initializate cele k threaduri, iar in al doilea for este apelata metoda start() pentru fiecare
		 * thread care apeleaza metoda run implementata in clasa Method1. Pentru a putea afisa timpul de executie al metodei, apelam
		 * functia {@link #Join(Thread[], int)} pentru a pune thredurile intr-o stare de asteptare, apoi afisam timpul.
		 * 
		 * @param n - limita maxima din intervalul [1,n]
		 * @param threads_number - numarul maxim de threaduri
		 */
		
		public static void FirstMethod(int n, int threads_number)
		{
			 Instant start1=Instant.now();	  
			 
			 int repose =n/threads_number;
				  
			 int rest=n%repose;
				  
			 Thread[] threads=new Thread[threads_number+1];
				  
			 threads[1]=new  Thread(new Method1(1,repose+1));
				  
			 for(int iterator=2;iterator<=threads_number;iterator++) { 				 
					  
				 threads[iterator]=new  Thread(new Method1((iterator-1)*repose+rest+1,iterator*repose+rest));
					  
			 }
			  
			 for(int iterator=1;iterator<=threads_number;iterator++) { 
					 
				 threads[iterator].start();
					  
			 }
				 				  
			 Instant finish1=Instant.now(); 
			 
			 Join(threads,threads_number); 
		
			 System.out.println("\n Duration: "+Duration.between(start1, finish1).toMillis() + " milliseconds");
				  
			
		}
		
		/**
		 * Functia Join apeleaza metoda join pentru fiecare thread din vectorul de threduri dat ca parametru, metoda
		 * care pune in asteptare fiecare thread pentru a nu amesteca outputului primei metode cu cel al celei de-a
		 * doua metode.
		 * <p>
		 * @param threads - vectorul de threaduri
		 * @param length - numarul de threaduri din vector
		 */
		
		public static void Join(Thread[] threads, int length)
		{
			for(int iterator=1;iterator<=length;iterator++) { 
			
				try {
				
					threads[iterator].join();
				
				}catch (InterruptedException exception) {
				
					exception.printStackTrace();
				
				}
			}
		}

}
