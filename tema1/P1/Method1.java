	
	/**
	 * <strong>Class Method1</strong>
	 * <p>
	 * Clasa implementeaza interfata Runnable si descrie prima metoda de rezolvare
	 * a problemei date. Membrii de tip data sunt <strong>min_limit</strong>, <strong>max_limit</strong> care
	 * definesc limita inferioara si limita superioara a intervalului threadului si stringul <strong>threadName</strong>
	 * care reprezinta numarul firului pentru o descriere mai buna a outputului.
	 * <p>
	 * {@link #Method1(int, int, String)} - constructorul clasei<p>
	 * {@link #run()} - implementarea functiei run<p>
	 * {@link #primeTest(Integer)} - implementare algoritmului de testare a primalitatii<p>
	 * <p>
	 * @author Marinescu Ana
	 * @version 23.10.2019
	 */

	public class Method1 implements Runnable{
		
		private int min_limit;
		
		private int max_limit;		
	
		/**
		 * Constructorul clasei care initializeaza variabilele astfel:
		 * @param min - limita inferioara a intervalului threadului
		 * @param max - limita maxima a intervalului
		 */
		
		public Method1(int min,int max)
	    {
			min_limit=min;
			
	    	max_limit=max;
	    	
	    }
	    
		/**
		 * Functia care verifica daca numarul dat ca parametru este sau nu un numar prim astfel:
		 * numarul dat este impartit pe rand la toate numerele din intervalul [3,numarul/2], iar daca se gaseste un
		 * multiplu de-al lui, atunci este considerat ca nefiind prim, altfel el este prim. De asemenea, intervalul porneste 
		 * de la 3 deoarece 1 nu este numar prim, verificarea cu 2 se face inainte de a apela functia.<p>
		 * 
		 * @param number - numarul pentru care se face testul de primalitate
		 * @return false daca numarul nu este prim si true in caz contrar
		 */
		
		private Boolean primeTest(Integer number)
		{
			
			for(int iterator=3;iterator<number/2;iterator++)
			{
				if(number%iterator == 0)
				{
					return false;
				}
			}
			
			return true;
		}
		
		/**
		 * Metoda suprascrie functia run a interfetei Runnable. Sunt parcurse toate numerele din interval cu un iterator cu acelas nume. Daca iterator e chiar doi,
		 * atunci este afisat deoarece 2 este numar prim, in caz contrar, adica numarul este mai mare ca 2 si nu 
		 * este numar par (numerele pare nu sunt prime), este apelata functia primeTest descrisa anterior.
		 * Cand functia returneaza true, atunci inseamna ca numarul este prim si acesta este afisat in consola,
		 * altfel este sarit.<p>
		 * 
		 * {@link #primeTest(Integer)} 
		 */
		@Override
		public void run() {			
			
			for(int iterator=min_limit;iterator<=max_limit;iterator++)
			{
				if(iterator==2)
				{
					System.out.println(iterator);
				}
				
				if(iterator%2!=0 && iterator>2)
				{
					if(primeTest(iterator)==true)
					{
						System.out.println(iterator);
					}
				}
			}
		}
		
	}
