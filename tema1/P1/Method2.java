	import java.util.Vector;

	/**
	 * <strong>Class Method2</strong>
	 * <p>
	 * Clasa implementeaza interfata Runnable si descrie a doua metoda de rezolvare
	 * a problemei date. Ca membrii de tip data avem un vector <strong>subset</strong> de tip Integer care
	 * contine toate numerele din intervalul threadului curent.
	 * <p>
	 * {@link #Method2(Vector)} - constructorul clasei<p>
	 * {@link #run()} - implementarea functiei run<p>
	 * {@link #primeTest(Integer)} - implementare algoritmului de testare a primalitatii<p>
	 * <p>
	 * @author Marinescu Ana
	 * @version 23.10.2019
	 */
	
	public class Method2 implements Runnable {
			
		Vector<Integer> subset;
			
		/**
		 * Constructorul clasei Method2 care initializeaza multimea threadului, adica vectorul subset.
		 * <p>
		 * @param array - vectorul care cuprinde numerele din multimea threadului curent
		 */
		public Method2(Vector<Integer> array) {
				
			subset=array;
			
		}
		
		/**
		 * Functia care verifica daca numarul dat ca parametru este sau nu un numar prim, astfel:
		 * numarul dat este impartit pe rand la toate numerele din intervalul [3,numarul/2], iar daca se gaseste un
		 * multiplu de-al lui, atunci este considerat ca nefiind prim, altfel el este prim. De asemenea, intervalul porneste 
		 * de la 3 deoarece 1 nu este numar prim,iar verificarea cu 2 se face inainte de a apela functia, si se termina la numarul/2
		 * numerele mai mari ca el, odata impartite la numarul dat, vor da automat un rest diferit de 0 la impartire.<p>
		 * 
		 * @param number - numarul pentru care se face testul de primalitate
		 * @return false daca numarul nu este prim si true in caz contrar
		 */
		
		private Boolean primeTest(Integer number)
		{
			for(int iterator=3;iterator<number/2;iterator++)
			{
				if(number%iterator==0)
				{
					return false;
				}
			}
			
			return true;
		}
			
		/**
		 * Metoda suprascrie functia run a interfetei Runnable. Sunt parcurse toate numerele din interval 
		 * cu un iterator numit integer. Daca integer e chiar doi, atunci este afisat deoarece 2 este numar prim, 
		 * in caz contrar, adica numarul este mai mare ca 2 si nu este numar par 
		 * (numerele pare diferite de 2 nu sunt prime), este apelata functia primeTest descrisa anterior.
		 * Cand functia returneaza true, atunci inseamna ca numarul este prim si acesta este afisat in consola,
		 * altfel este sarit.<p>
		 * 
		 * {@link #primeTest(Integer)} 
		 */
		@Override
		public void run() {
			
			for (Integer integer : subset) {
				
				if(integer==2)
				{
					System.out.println(integer);
				}					
				else if(integer%2!=0 && integer>2)
				{
					if(primeTest(integer)==true)
					{
						System.out.println(integer);
					}
				}
			}			
		}
		
	}
